package event

type TokenCache interface {
	Get(account string) (token string, err error)
	Set(account string, token string) (err error)
}
