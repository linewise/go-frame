package event

import (
	"context"
	"errors"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/util"
	"sync"
	"time"
)

type (
	Hook func(ctx context.Context, data any) ([]byte, error)

	Event struct {
		Identity string    `json:"identity"` // 平台名
		Name     string    `json:"name"`     // 事件名称
		Hook     Hook      `json:"hook"`
		Created  time.Time `json:"created"`
	}

	Channel struct {
		Ctx      context.Context
		Name     string      `json:"name"`     // 事件类型
		Identity string      `json:"identity"` // 身份
		InData   []byte      `json:"in_data"`  // 输入数据
		OutData  chan []byte `json:"out_data"` // 回传数据
	}

	ServerConfig struct {
		Name         string
		PoolSize     int64 `mapstructure:"poolSize"`
		ReadTimeout  int64 `mapstructure:"readTimeout"`
		WriteTimeout int64 `mapstructure:"writeTimeout"`
	}

	Server struct {
		Config    ServerConfig
		Events    map[string]map[string][]*Event // 事件列表
		channel   chan *Channel
		ctx       context.Context
		ctxCancel context.CancelFunc
	}
)

// NewServer 事件的服务端
func NewServer(config ServerConfig) *Server {
	ctx, cancel := context.WithCancel(context.Background())

	// 配置设置
	if config.ReadTimeout <= 0 {
		config.ReadTimeout = constants.DefaultTimeout
	}
	if config.WriteTimeout <= 0 {
		config.WriteTimeout = constants.DefaultTimeout
	}
	if config.PoolSize <= 0 {
		config.PoolSize = constants.DefaultServerMinPool
	}

	return &Server{
		Config:    config,
		Events:    make(map[string]map[string][]*Event),
		channel:   make(chan *Channel, config.PoolSize),
		ctx:       ctx,
		ctxCancel: cancel,
	}
}

// Run 执行任务
func (s *Server) Run() error {
	syncChan := util.NewSyncChanUtil(s.Config.PoolSize)

	for {
		select {
		// 结束通道
		case <-s.ctx.Done():
			logger.Logger.Warnf("任务服务结束...")
			return nil

		// 业务
		case call := <-s.channel:
			if _, ok := s.Events[call.Name]; !ok {
				logger.Logger.Warnf("任务列表不存在，事件：%s，用户：%s", call.Name, call.Identity)
				call.OutData <- []byte("任务列表不存在，事件：" + call.Name)
				continue
			}

			syncChan.Add()
			go func(lists map[string][]*Event, call *Channel) {
				timeNow := time.Now().UnixMilli()
				defer func() {
					logger.Logger.Infof("全部耗时：%d毫秒", time.Now().UnixMilli()-timeNow)
					syncChan.Done()
				}()

				// 运行各个事件
				callInData := call.InData // 获取传入数据
				syncChanTpl := sync.WaitGroup{}
				for identity, items := range lists {
					if identity != call.Identity {
						for _, item := range items {
							syncChanTpl.Add(1)
							go func(item *Event) {
								defer syncChanTpl.Done()

								_, err := item.Hook(call.Ctx, callInData)
								if err != nil {
									logger.Logger.Errorf("调用身份存在错误：%s，事件：%s，错误：%s，传入数据：%s", item.Identity, item.Name, err, callInData)
									return
								}
							}(item)
						}
					}
				}

				syncChanTpl.Wait()
				call.OutData <- []byte("请求成功了")

			}(s.Events[call.Name], call)
		}
	}

	syncChan.Wait()
	return nil
}

// Register 注册方法
func (s *Server) Register(event *Event) error {
	if _, ok := s.Events[event.Name]; !ok {
		s.Events[event.Name] = make(map[string][]*Event)
		s.Events[event.Name][event.Identity] = append(s.Events[event.Name][event.Identity], event)
		return nil
	}

	s.Events[event.Name][event.Identity] = append(s.Events[event.Name][event.Identity], event)
	return nil
}

// Trigger 触发方法
func (s *Server) Trigger(call *Channel) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(s.Config.WriteTimeout)*time.Second)
	defer cancel()

	select {
	case s.channel <- call:
	case <-ctx.Done():
		logger.Logger.Warnf("任务数量太多，通道满仓了")
	}
}

// GetChannel 等待触发返回数据
func (s *Server) GetChannel(call *Channel) (data []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(s.Config.ReadTimeout)*time.Second)
	defer cancel()

	select {
	case data = <-call.OutData:
	case <-ctx.Done():
		logger.Logger.Warnf("触发任务超时，服务端响应太慢。")
		err = errors.New("接收数据超时")
	}

	return data, err
}

func (s *Server) Close() {
	s.ctxCancel()
}
