package event

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestNewServer(t *testing.T) {
	var (
		taskRd = &Event{
			Name:     "add_user",
			Identity: "rd",
			Created:  time.Now(),
			Hook: func(ctx context.Context, data any) ([]byte, error) {
				t.Log("add rd user success.")
				t.Log(fmt.Sprint(data))
				return nil, nil
			},
		}
		taskMos = &Event{
			Name:     "add_user",
			Identity: "mos",
			Created:  time.Now(),
			Hook: func(ctx context.Context, data any) ([]byte, error) {
				t.Log("add mos user success.")
				t.Log(string(data.([]byte)))
				return []byte("王龙军"), nil
			},
		}
		server = NewServer(ServerConfig{
			Name:         "task",
			PoolSize:     100,
			ReadTimeout:  10,
			WriteTimeout: 20,
		})
	)

	// 注册
	server.Register(taskRd)
	server.Register(taskMos)

	// 启动
	go server.Run()

	// 触发
	for i := 0; i < 10; i++ {
		call := &Channel{
			Name:     "add_user",
			Identity: "rd",
			InData:   []byte(fmt.Sprint(i)),
			OutData:  make(chan []byte, 1),
		}
		server.Trigger(call)
		data, err := server.GetChannel(call)
		t.Log(string(data), err)
	}

	// 关闭
	server.Close()

	// 测试新的触发，获取数据超时
	call := &Channel{
		Name:     "add_user",
		Identity: "rd",
		InData:   []byte("123"),
		OutData:  make(chan []byte, 1),
	}
	server.Trigger(call)
	data, err := server.GetChannel(call)
	t.Log(string(data), err)

	t.Log("等待时间结束...")
	time.Sleep(time.Minute)
}
