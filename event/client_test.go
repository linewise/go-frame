package event

import (
	"encoding/json"
	"gitee.com/linewise/go-frame/event/cache"
	"testing"
)

func TestTaskServer_GetToken(t *testing.T) {
	taskServer := Client{
		Config: ClientConfig{
			Url:    "http://127.0.0.1:38000",
			Key:    "4kCFUbSiMGrATaGG",
			Secret: "WTYlj5S57MD3pXHn0XSyPDt9gOGDgcn9",
		},
		TokenCache: cache.NewMemory(),
	}

	t.Log(taskServer.GetToken())
}

func TestTaskServer_Trigger(t *testing.T) {
	taskServer := Client{
		Config: ClientConfig{
			Url:    "http://127.0.0.1:38000",
			Key:    "4kCFUbSiMGrATaGG",
			Secret: "WTYlj5S57MD3pXHn0XSyPDt9gOGDgcn9",
		},
		TokenCache: cache.NewMemory(),
	}

	dataBytes, _ := json.Marshal(struct {
		Name   string
		Phones []string
	}{
		Name:   "王龙军",
		Phones: []string{"18782783024"},
	})

	t.Log(taskServer.HttpTrigger("migrate:user", dataBytes))
}
