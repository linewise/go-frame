package event

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitee.com/linewise/go-frame/event/cache"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"io"
	"net/http"
	"strings"
	"time"
)

type (
	ClientConfig struct {
		Url      string `json:"url" mapstructure:"url"`
		Key      string `json:"key" mapstructure:"key"`
		Secret   string `json:"secret" mapstructure:"secret"`
		PoolSize int64  `mapstructure:"poolSize"`
	}

	Client struct {
		Config     ClientConfig
		Errs       []error
		ErrChannel chan error
		TokenCache
	}
)

func NewClient(config ClientConfig) *Client {
	// 配置设置
	if config.PoolSize <= 0 {
		config.PoolSize = constants.DefaultServerMinPool
	}

	client := &Client{
		Config:     config,
		Errs:       make([]error, 0),
		ErrChannel: make(chan error),
		TokenCache: cache.NewMemory(),
	}
	go client.CheckErr()

	return client
}

func (c Client) CheckErr() {
	var (
		startTime = time.Now().Minute()
		nowTime   = time.Now().Minute()
	)

	for {
		select {
		case err := <-c.ErrChannel:
			nowTime = time.Now().Minute()
			if nowTime-startTime > 5 {
				c.Errs = make([]error, 0)
			}

			c.Errs = append(c.Errs, err)
			startTime = nowTime
			if len(c.Errs) >= int(c.Config.PoolSize) {
				logger.Logger.Errorf("err length to big, count: %d", len(c.Errs))
				// TODO...
				// 发送消息
			}
		}
	}
}

// GetToken 获取token
func (c Client) GetToken() (string, error) {
	// 缓存中获取token
	if token, err := c.TokenCache.Get(c.Config.Key); err == nil && token != "" {
		return token, nil
	} else {
		// token失效了，重新获取
		urlStr := c.Config.Url + "/login"
		payload := strings.NewReader(`{"key": "` + c.Config.Key + `", "secret": "` + c.Config.Secret + `"}`)
		dataMap := make(map[string]any)
		resp, err := http.Post(urlStr, "application/json", payload)
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()

		// 读取返回数据
		bodyJsonData, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}
		if !json.Valid(bodyJsonData) {
			logger.Logger.Errorf("解析token数据错误：%s", bodyJsonData)
			return "", errors.New("请求返回数据异常")
		}
		if err := json.Unmarshal(bodyJsonData, &dataMap); err != nil {
			logger.Logger.Errorf("解析token数据错误：%s", bodyJsonData)
			return "", err
		}
		if dataMap["data"] == nil {
			logger.Logger.Errorf("token请求结果异常：%s", bodyJsonData)
			return "", errors.New("token请求结果异常")
		}

		dataVal := dataMap["data"].(map[string]any)
		return dataVal["token"].(string), c.TokenCache.Set(c.Config.Key, dataVal["token"].(string))
	}
}

// HttpTrigger 触发任务
func (c Client) HttpTrigger(event string, paramsByte []byte) (any, error) {
	if !json.Valid(paramsByte) {
		return nil, errors.New("传入参数不是json")
	}

	// 获取token
	token, err := c.GetToken()
	if err != nil {
		logger.Logger.Errorf("获取token错误：%v", err)
		return nil, err
	}

	// 请求数据
	client := &http.Client{}
	urlStr := c.Config.Url + "/event/transfer?event=" + event
	payload := bytes.NewReader(paramsByte)
	dataMap := make(map[string]any)
	for {
		req, err := http.NewRequest("POST", urlStr, payload)
		req.Header.Set("Authorization", token)
		req.Header.Set("Content-Type", "application/json")
		resp, err := client.Do(req)
		if err != nil {
			c.ErrChannel <- err
			resp.Body.Close()
			time.Sleep(5 * time.Second)
			continue
		}

		if resp.StatusCode == http.StatusOK {
			bodyJsonData, err := io.ReadAll(resp.Body)
			if err != nil {
				c.ErrChannel <- err
				resp.Body.Close()
				time.Sleep(5 * time.Second)
				continue
			}
			if err := json.Unmarshal(bodyJsonData, &dataMap); err != nil {
				logger.Logger.Errorf("code:%v", resp.StatusCode)
				logger.Logger.Errorf("token:%s", token)
				logger.Logger.Errorf("url:%s", urlStr)
				logger.Logger.Errorf("解析json数据失败：%s", bodyJsonData)
				c.ErrChannel <- err
				resp.Body.Close()
				time.Sleep(5 * time.Second)
				continue
			}
			resp.Body.Close()
			break
		}

		logger.Logger.Errorf("服务器请求失败, code:%v", resp.StatusCode)
		c.ErrChannel <- errors.New("服务器请求失败")
		resp.Body.Close()
		time.Sleep(5 * time.Second)
	}

	return dataMap, nil
}
