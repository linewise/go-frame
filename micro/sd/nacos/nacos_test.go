package nacos

import "testing"

func TestNacos_GetConfig(t *testing.T) {
	nacosServerConfig := []ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := NewNacos(nacosServerConfig, nacosClientConfig)

	t.Log(nacos.GetConfig("emt-user-center", ""))
}

func TestNacos_Listen(t *testing.T) {
	nacosServerConfig := []ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := NewNacos(nacosServerConfig, nacosClientConfig)

	t.Log(nacos.Listen("local", "emt-user-center", func(data string) error {
		t.Log("data:", data)
		return nil
	}))

	// 程序等待
	var await chan bool = make(chan bool)
	<-await
}

func TestNacos_Register(t *testing.T) {
	nacosServerConfig := []ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := NewNacos(nacosServerConfig, nacosClientConfig)

	t.Log(nacos.Register(
		RegisterConfig{
			Ip:          "127.0.0.1",
			Port:        uint64(18000),
			ServiceName: "test",
			GroupName:   "test",
			Weight:      10,
			Enable:      true,
			Healthy:     true,
			Ephemeral:   true,
		}),
	)
}

func TestNacos_Register1(t *testing.T) {
	nacosServerConfig := []ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := NewNacos(nacosServerConfig, nacosClientConfig)

	t.Log(nacos.Register(
		RegisterConfig{
			Ip:          "127.0.0.1",
			Port:        uint64(18000),
			ServiceName: "test",
			GroupName:   "test",
			Weight:      10,
			Enable:      true,
			Healthy:     true,
			Ephemeral:   true,
		}),
	)
}
