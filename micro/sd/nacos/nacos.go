package nacos

import (
	"errors"
	"gitee.com/linewise/go-frame/logger"
	"github.com/nacos-group/nacos-sdk-go/v2/clients"
	"github.com/nacos-group/nacos-sdk-go/v2/clients/config_client"
	"github.com/nacos-group/nacos-sdk-go/v2/clients/naming_client"
	"github.com/nacos-group/nacos-sdk-go/v2/common/constant"
	"github.com/nacos-group/nacos-sdk-go/v2/vo"
	"path/filepath"
)

type (
	ClientConfig struct {
		Username            string `json:"username" mapstructure:"username"`
		Password            string `json:"password" mapstructure:"password"`
		NamespaceId         string `json:"namespaceId" mapstructure:"namespaceId"`
		TimeoutMs           uint64 `json:"timeoutMs" mapstructure:"timeoutMs"`
		NotLoadCacheAtStart bool   `json:"notLoadCacheAtStart" mapstructure:"notLoadCacheAtStart"`
		LogDir              string `json:"logDir" mapstructure:"logDir"`
		CacheDir            string `json:"cacheDir" mapstructure:"cacheDir"`
		LogLevel            string `json:"logLevel" mapstructure:"logLevel"`
	}
	ServerConfig struct {
		Scheme      string `json:"scheme" mapstructure:"scheme"`
		ContextPath string `json:"contextPath" mapstructure:"contextPath"`
		IpAddr      string `json:"ipAddr" mapstructure:"ipAddr"`
		Port        uint64 `json:"port" mapstructure:"port"`
		GrpcPort    uint64 `json:"grpcPort" mapstructure:"grpcPort"`
	}
	RegisterConfig   vo.RegisterInstanceParam
	RegisterInstance map[string]vo.RegisterInstanceParam

	Nacos struct {
		ClientConfig     ClientConfig
		ServerConfigs    []ServerConfig
		RegisterInstance RegisterInstance // 已注册服务的配置
		NamingClient     naming_client.INamingClient
		ConfigClient     config_client.IConfigClient
	}
)

// NewNacos 实例化nacos
func NewNacos(s []ServerConfig, c ClientConfig) (*Nacos, error) {
	// 初始化配置
	sp := string(filepath.Separator)
	fileDir, _ := filepath.Abs(filepath.Dir(filepath.Join(".")))
	if c.LogDir == "" {
		c.LogDir = fileDir + sp + "logs" + sp
	}
	if c.CacheDir == "" {
		c.CacheDir = fileDir + sp + "caches" + sp
	}
	if c.LogLevel == "" {
		c.LogLevel = logger.ZapLog.GetConfig().Level.String()
	}
	if c.LogLevel == "" {
		c.LogLevel = "info"
	}

	sc := make([]constant.ServerConfig, 0)
	for _, config := range s {
		sc = append(sc, constant.ServerConfig{
			IpAddr:      config.IpAddr,
			ContextPath: config.ContextPath,
			Port:        config.Port,
			GrpcPort:    config.GrpcPort,
			Scheme:      config.Scheme,
		})
	}

	nacosClientParam := vo.NacosClientParam{
		ClientConfig: &constant.ClientConfig{
			Username:            c.Username,
			Password:            c.Password,
			NamespaceId:         c.NamespaceId,
			TimeoutMs:           c.TimeoutMs,
			NotLoadCacheAtStart: c.NotLoadCacheAtStart,
			LogDir:              c.LogDir,
			CacheDir:            c.CacheDir,
			LogLevel:            c.LogLevel,
		},
		ServerConfigs: sc,
	}

	// 实例化
	namingClient, err := clients.NewNamingClient(nacosClientParam)
	if err != nil {
		return nil, err
	}

	configClient, err := clients.NewConfigClient(nacosClientParam)
	if err != nil {
		return nil, err
	}

	return &Nacos{
		ClientConfig:     c,
		ServerConfigs:    s,
		RegisterInstance: make(map[string]vo.RegisterInstanceParam),
		NamingClient:     namingClient,
		ConfigClient:     configClient,
	}, nil
}

// Register 服务注册
func (n Nacos) Register(c RegisterConfig) (bool, error) {
	// 服务注册
	success, err := n.NamingClient.RegisterInstance(vo.RegisterInstanceParam{
		Ip:          c.Ip,
		Port:        c.Port,
		ServiceName: c.ServiceName,
		Weight:      c.Weight,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
		Metadata:    c.Metadata,
		GroupName:   c.GroupName,
		ClusterName: c.ClusterName,
	})
	if err != nil {
		return false, err
	}

	// 缓存注册服务信息
	n.RegisterInstance[c.ServiceName] = vo.RegisterInstanceParam{
		Ip:          c.Ip,
		Port:        c.Port,
		ServiceName: c.ServiceName,
		Weight:      c.Weight,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
		Metadata:    c.Metadata,
		GroupName:   c.GroupName,
		ClusterName: c.ClusterName,
	}
	return success, nil
}

// Deregister 服务注销
func (n Nacos) Deregister(serverName string) (bool, error) {
	if param, ok := n.RegisterInstance[serverName]; ok {
		success, err := n.NamingClient.DeregisterInstance(vo.DeregisterInstanceParam{
			Ip:          param.Ip,
			Port:        param.Port,
			ServiceName: serverName,
			Ephemeral:   true,
			Cluster:     param.ClusterName,
			GroupName:   param.GroupName,
		})

		return success, err
	}

	return false, errors.New("服务配置不存在")
}

// GetOneServer 获取有效的服务信息
func (n Nacos) GetOneServer(serverName string) (ip string, port uint64, err error) {
	if param, ok := n.RegisterInstance[serverName]; ok {
		instance, err := n.NamingClient.SelectOneHealthyInstance(vo.SelectOneHealthInstanceParam{
			ServiceName: serverName,
			GroupName:   param.GroupName,
			Clusters:    []string{param.ClusterName},
		})
		if err != nil {
			return "", 0, err
		}

		return instance.Ip, instance.Port, nil
	}

	instance, err := n.NamingClient.SelectOneHealthyInstance(vo.SelectOneHealthInstanceParam{
		ServiceName: serverName,
	})
	if err == nil {
		return instance.Ip, instance.Port, nil
	}

	return "", 0, errors.New("服务配置不存在")
}

// GetConfig 获取配置
func (n Nacos) GetConfig(id, group string) (string, error) {
	if group == "" {
		group = constant.DEFAULT_GROUP
	}

	return n.ConfigClient.GetConfig(vo.ConfigParam{
		DataId: id,
		Group:  group})
}

// Listen 监听配置
func (n Nacos) Listen(id, group string, callbackFunc func(data string) error) error {
	if group == "" {
		group = constant.DEFAULT_GROUP
	}

	return n.ConfigClient.ListenConfig(vo.ConfigParam{
		DataId: id,
		Group:  group,
		OnChange: func(namespace, group, dataId, data string) {
			logger.Logger.Infof("group: %s, dataId: %s, data: %s", group, dataId, data)
			if err := callbackFunc(data); err != nil {
				logger.Logger.Errorf("配置文件回调错误: %v", err)
			}
		},
	})
}
