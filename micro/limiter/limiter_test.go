package limiter

import (
	"context"
	"gitee.com/linewise/go-frame/micro/endpoint"
	"testing"
)

func TestLimiter_Middleware(t *testing.T) {
	limiter := NewLimiter(Config{Rate: 100})
	t.Log(limiter.Endpoint(endpoint.NilEndpoint())(context.Background(), nil))
}
