package limiter

import (
	"context"
	"gitee.com/linewise/go-frame/micro/endpoint"
	"go.uber.org/ratelimit"
)

type (
	Config struct {
		Rate int `mapstructure:"rate"`
	}

	Limiter struct {
		Config
		Limiter ratelimit.Limiter
	}
)

// NewLimiter 限流器
func NewLimiter(c Config) *Limiter {
	return &Limiter{
		Config:  c,
		Limiter: ratelimit.New(c.Rate),
	}
}

func (l Limiter) Limit() {
	l.Limiter.Take()
}

func (l Limiter) Endpoint(next endpoint.Endpoint) endpoint.Endpoint {
	return func(ctx context.Context, request any) (response any, err error) {
		l.Limiter.Take()
		return next(ctx, request)
	}
}

func (l Limiter) Middleware() endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request any) (response any, err error) {
			l.Limiter.Take()
			return next(ctx, request)
		}
	}
}
