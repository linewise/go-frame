package endpoint

import (
	"context"
)

type (
	Endpoint   func(ctx context.Context, req any) (res any, err error)
	Middleware func(endpoint Endpoint) Endpoint
)

func NilEndpoint() Endpoint {
	return func(ctx context.Context, req any) (res any, err error) {
		return nil, nil
	}
}

func DefaultEndpoint() Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		return request, nil
	}
}
