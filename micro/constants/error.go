package constants

import "errors"

var (
	ErrServerConfig        = errors.New("config error")
	ErrServerPort          = errors.New("server port error")
	ErrServerConnManyToMax = errors.New("server conn to many")
	ErrSetDeadline         = errors.New("websocket: cannot set deadline: not using a net.Conn")
	ErrDecodeParams        = errors.New("decode err by params")
	ErrHandlerTimeout      = errors.New("handler timeout")
	ErrNeedPtr             = errors.New("need param id ptr")
)
