package constants

const (
	DefaultTimeout       int64 = 30
	DefaultServerMaxPool int64 = 10000
	DefaultServerMinPool int64 = 100
	DefaultClientPool    int64 = 10
	DefaultRetryCount    int64 = 3
)
