package metrics

import "github.com/prometheus/client_golang/prometheus"

type (
	Metrics struct {
		counter   prometheus.Counter
		histogram prometheus.Histogram
	}
)
