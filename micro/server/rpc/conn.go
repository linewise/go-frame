package rpc

import (
	"encoding/json"
	"errors"
	"gitee.com/linewise/go-frame/logger"
	"io"
	"net"
	"net/rpc"
	"sync"
)

type Conn struct {
	server   *Server
	decoder  *json.Decoder
	encoder  *json.Encoder
	closer   net.Conn
	mutex    sync.Mutex
	sequence uint64 // 序列号
	pending  map[uint64]Request
}

func NewConn(conn net.Conn, s *Server) rpc.ServerCodec {
	return &Conn{
		server:  s,
		decoder: json.NewDecoder(conn),
		encoder: json.NewEncoder(conn),
		closer:  conn,
		pending: make(map[uint64]Request),
	}
}

func (c *Conn) ReadRequestHeader(r *rpc.Request) error {
	// 接收数据
	var request Request
	err := c.decoder.Decode(&request)
	if err == io.EOF {
		logger.Logger.Warnf("客户端连接已断开: %v", err)
		return err
	}
	if err != nil {
		logger.Logger.Errorf("rpc: request请求解析数据失败: %v", err)
		return err
	}

	// 设置rpc
	r.ServiceMethod = request.Method
	c.mutex.Lock()
	c.sequence++
	c.pending[c.sequence] = request
	r.Seq = c.sequence
	c.mutex.Unlock()

	return nil
}

func (c *Conn) ReadRequestBody(x any) error {
	if x == nil {
		return nil
	}
	if c.pending[c.sequence].Params == nil {
		return errors.New("rpc: request中请求缺少params参数")
	}

	params := [1]any{x}
	return json.Unmarshal(*c.pending[c.sequence].Params, &params)
}

func (c *Conn) WriteResponse(r *rpc.Response, x any) error {
	c.mutex.Lock()
	b, ok := c.pending[r.Seq]
	if !ok {
		c.mutex.Unlock()
		return errors.New("rpc: response中的序列号无效")
	}
	delete(c.pending, r.Seq)
	c.mutex.Unlock()

	return c.encoder.Encode(Response{
		Id:     b.Id,
		Result: x,
		Error:  nil,
	})
}

func (c *Conn) Close() error {
	c.closer.Close()
	c.server.delConn(&c.closer)
	return nil
}
