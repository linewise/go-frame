package rpc

import (
	"context"
	"fmt"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"net/rpc/jsonrpc"
	"reflect"
)

type (
	ClientConfig struct {
		Retry   int64 `mapstructure:"retry"` // 重试次数
		Timeout int64 `mapstructure:"timeout"`
	}

	Client struct {
		Config ClientConfig
		Nacos  *nacos.Nacos
	}
)

func NewClient(n *nacos.Nacos) *Client {
	return &Client{Nacos: n}
}

func (c *Client) init() error {
	// 初始化配置文件
	if c.Config.Timeout == 0 {
		c.Config.Timeout = constants.DefaultTimeout
	}
	if c.Config.Retry == 0 {
		c.Config.Retry = constants.DefaultRetryCount
	}
	return nil
}

func (c *Client) Do(ctx context.Context, serverName string, method string, req any, res any) error {
	if reflect.TypeOf(res).Kind() != reflect.Ptr {
		return constants.ErrNeedPtr
	}

	if err := c.init(); err != nil {
		return err
	}

	// 服务发现
	ip, port, err := c.Nacos.GetOneServer(serverName)
	if err != nil {
		return err
	}

	// 建立连接
	url := fmt.Sprintf("%s:%d", ip, port)
	rpcClient, err := jsonrpc.Dial("tcp", url)
	if err != nil {
		return err
	}

	for times := 1; times <= int(c.Config.Retry); times++ {
		err = rpcClient.Call(method, req, res)
		if err == nil {
			return nil
		}
		logger.Logger.Infof("%s第%d次请求，err：%v", url, times, err)
	}
	return nil
}
