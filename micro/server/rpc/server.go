package rpc

import (
	"errors"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/micro/limiter"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"net"
	"net/rpc"
	"reflect"
	"strconv"
	"sync"
)

type (
	ServerConfig struct {
		Host         string `mapstructure:"host"`
		Port         string `mapstructure:"port"`
		ReadTimeout  int64  `mapstructure:"readTimeout"`
		WriteTimeout int64  `mapstructure:"writeTimeout"`
		Metadata     map[string]string
	}

	Server struct {
		Identity     string
		IdentityName string
		Environment  string
		Name         string
		Config       ServerConfig
		Handlers     []Handler
		Limiter      *limiter.Limiter
		Nacos        *nacos.Nacos

		mu          sync.Mutex
		activeConns map[*net.Conn]rpc.ServerCodec
	}
)

// NewServer rpc的服务端
func NewServer(identity, identityName, environment, name string, c ServerConfig, l *limiter.Limiter, n *nacos.Nacos) *Server {
	return &Server{
		Identity:     identity,
		IdentityName: identityName,
		Environment:  environment,
		Name:         name,
		Config:       c,
		Limiter:      l,
		Nacos:        n,
	}
}

func (s *Server) init() error {
	// 配置文件检测
	if s.Config.Host == "" || s.Config.Port == "" || s.Name == "" {
		return constants.ErrServerConfig
	}

	// 配置文件设置
	if s.Config.ReadTimeout == 0 {
		s.Config.ReadTimeout = constants.DefaultTimeout
	}
	if s.Config.WriteTimeout == 0 {
		s.Config.WriteTimeout = constants.DefaultTimeout
	}
	s.Config.Metadata = map[string]string{"type": "rpc"}
	s.activeConns = make(map[*net.Conn]rpc.ServerCodec)
	return nil
}

func (s *Server) Run() error {
	// 检测和初始化配置文件
	if err := s.init(); err != nil {
		return err
	}

	// 监听端口
	addr, err := net.ResolveTCPAddr("tcp4", s.Config.Host+":"+s.Config.Port)
	if err != nil {
		return err
	}
	listen, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return err
	}

	// 服务注册
	port, err := strconv.Atoi(s.Config.Port)
	if err != nil {
		return errors.New("port错误")
	}
	success, err := s.Nacos.Register(nacos.RegisterConfig{
		Ip:          s.Config.Host,
		Port:        uint64(port),
		ServiceName: s.Name,
		GroupName:   s.IdentityName + "(" + s.Environment + ")",
		Weight:      10,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
		Metadata:    s.Config.Metadata,
	})
	logger.Logger.Infof("服务注册：%v, err: %v", success, err)

	// rpc注册
	for _, handler := range s.Handlers {
		if handler.Method == "" {
			if err := rpc.Register(handler.Receiver); err != nil {
				logger.Logger.Errorf("注册rpc失败, err: %v", err)
			}
			continue
		}

		if err := rpc.RegisterName(handler.Method, handler.Receiver); err != nil {
			logger.Logger.Errorf("注册rpc失败, method: %s, err: %v", handler.Method, err)
		}
	}

	for {
		// 限流
		s.Limiter.Limit()

		// 接收连接
		conn, err := listen.Accept()
		if err != nil {
			continue
		}

		// 处理数据
		myConn := NewConn(conn, s)
		s.activeConns[&conn] = myConn
		go rpc.ServeCodec(myConn)
	}
}

func (s *Server) Register(method string, receiver any) error {
	receiverType := reflect.TypeOf(receiver)
	if receiverType.Kind() != reflect.Ptr {
		return errors.New("注册需要传入指针类型结构体")
	}

	if receiverType.Elem().Kind() != reflect.Struct {
		return errors.New("注册需要传入指针类型结构体")
	}

	s.Handlers = append(s.Handlers, Handler{
		Method:   method,
		Receiver: receiver,
	})
	return nil
}

func (s *Server) delConn(c *net.Conn) {
	if _, ok := s.activeConns[c]; ok {
		delete(s.activeConns, c)
	}
}

// Close 关闭服务和所有连接
func (s *Server) Close() {
	s.mu.Lock()
	defer s.mu.Unlock()

	for c := range s.activeConns {
		(*c).Close()
		delete(s.activeConns, c)
	}
}
