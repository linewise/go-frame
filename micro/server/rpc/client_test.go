package rpc

import (
	"context"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"gitee.com/linewise/go-frame/micro/server/rpc/examples"
	"testing"
)

func TestClient_Do(t *testing.T) {
	nacosServerConfig := []nacos.ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := nacos.ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := nacos.NewNacos(nacosServerConfig, nacosClientConfig)

	client := NewClient(nacos)
	ret := 0
	t.Log(client.Do(context.Background(), "test", "Rect.Area", examples.AreaRequest{30, 100}, &ret))
	t.Log(ret)
	t.Log(client.Do(context.Background(), "test", "Rect.Perimeter", examples.AreaRequest{30, 100}, &ret))
	t.Log(ret)
}
