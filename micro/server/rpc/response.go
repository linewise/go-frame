package rpc

import "encoding/json"

type Response struct {
	Id     *json.RawMessage
	Result any
	Error  any
}
