package examples

type RectTransport struct{}

func (r *RectTransport) Area(p AreaRequest, ret AreaResponse) error {
	*ret = p.Width * p.Height
	return nil
}

func (r *RectTransport) Perimeter(p PerimeterRequest, ret PerimeterResponse) error {
	*ret = (p.Width + p.Height) * 2
	return nil
}
