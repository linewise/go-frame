package examples

type (
	AreaRequest struct {
		Width, Height int
	}
	AreaResponse *int

	PerimeterRequest struct {
		Width, Height int
	}
	PerimeterResponse *int
)
