package websocket

import (
	"io"
)

type Codec struct {
	Marshal   func(v any) (data []byte, payloadType byte, err error)
	Unmarshal func(data []byte, payloadType byte, v any) (err error)
}

func (c Codec) Send(ws *Conn, v any) (err error) {
	data, payloadType, err := c.Marshal(v)
	if err != nil {
		return err
	}
	ws.wio.Lock()
	defer ws.wio.Unlock()
	w, err := ws.frameWriterFactory.NewFrameWriter(payloadType)
	if err != nil {
		return err
	}
	_, err = w.Write(data)
	w.Close()
	return err
}

func (c Codec) Receive(ws *Conn, v any) (err error) {
	ws.rio.Lock()
	defer ws.rio.Unlock()
	if ws.frameReader != nil {
		_, err = io.Copy(io.Discard, ws.frameReader)
		if err != nil {
			return err
		}
		ws.frameReader = nil
	}

again:
	frame, err := ws.frameReaderFactory.NewFrameReader()
	if err != nil {
		return err
	}
	frame, err = ws.frameHandler.HandleFrame(frame)
	if err != nil {
		return err
	}
	if frame == nil {
		goto again
	}
	maxPayloadBytes := ws.MaxPayloadBytes
	if maxPayloadBytes == 0 {
		maxPayloadBytes = DefaultMaxPayloadBytes
	}
	if hf, ok := frame.(*hybiFrameReader); ok && hf.header.Length > int64(maxPayloadBytes) {
		ws.frameReader = frame
		return ErrFrameTooLarge
	}
	payloadType := frame.PayloadType()
	data, err := io.ReadAll(frame)
	if err != nil {
		return err
	}
	return c.Unmarshal(data, payloadType, v)
}
