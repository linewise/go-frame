package websocket

type Response struct {
	Success bool   `json:"success"`
	Code    int    `json:"code"`
	Message string `json:"message,omitempty"`
	Data    any    `json:"datas,omitempty"`
}

// NewResponse 实例化
func NewResponse(success bool, code int, message string, data any) error {
	return &Response{Success: success, Code: code, Message: message, Data: data}
}

func (s Response) Error() string {
	return s.Message
}
