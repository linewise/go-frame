package websocket

import (
	"errors"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/micro/limiter"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"net/http"
	"strconv"
	"time"
)

type (
	ServerConfig struct {
		Host         string `mapstructure:"host"`
		Port         string `mapstructure:"port"`
		ReadTimeout  int64  `mapstructure:"readTimeout"`
		WriteTimeout int64  `mapstructure:"writeTimeout"`
		Metadata     map[string]string
	}

	Server struct {
		Identity     string
		IdentityName string
		Environment  string
		Name         string
		Config       ServerConfig
		Handler      http.Handler
		Limiter      *limiter.Limiter
		Nacos        *nacos.Nacos

		service *http.Server
	}
)

// NewServer websocket的服务端
func NewServer(identity, identityName, environment, name string, c ServerConfig, h http.Handler, l *limiter.Limiter, n *nacos.Nacos) *Server {
	return &Server{
		Identity:     identity,
		IdentityName: identityName,
		Environment:  environment,
		Name:         name,
		Config:       c,
		Handler:      h,
		Limiter:      l,
		Nacos:        n,
	}
}

func (s *Server) init() error {
	// 配置文件检测
	if s.Config.Host == "" || s.Config.Port == "" || s.Name == "" {
		return constants.ErrServerConfig
	}

	// 配置文件设置
	if s.Config.ReadTimeout == 0 {
		s.Config.ReadTimeout = constants.DefaultTimeout
	}
	if s.Config.WriteTimeout == 0 {
		s.Config.WriteTimeout = constants.DefaultTimeout
	}
	s.Config.Metadata = map[string]string{"type": "websocket"}
	return nil
}

func (s *Server) Run() error {
	// 检测和初始化配置文件
	if err := s.init(); err != nil {
		return err
	}

	s.service = &http.Server{
		Addr:         s.Config.Host + ":" + s.Config.Port,
		Handler:      s,
		ReadTimeout:  time.Duration(s.Config.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(s.Config.WriteTimeout) * time.Second,
	}

	// 服务注册
	port, err := strconv.Atoi(s.Config.Port)
	if err != nil {
		return errors.New("port错误")
	}
	success, err := s.Nacos.Register(nacos.RegisterConfig{
		Ip:          s.Config.Host,
		Port:        uint64(port),
		ServiceName: s.Name,
		GroupName:   s.IdentityName + "(" + s.Environment + ")",
		Weight:      10,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
		Metadata:    s.Config.Metadata,
	})
	logger.Logger.Infof("服务注册：%v, err: %v", success, err)

	return s.service.ListenAndServe()
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// 限流
	s.Limiter.Limit()

	// 调用业务
	s.Handler.ServeHTTP(w, r)
}

func (s *Server) Close() {
	s.service.Close()
}
