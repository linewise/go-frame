package websocket

import "net/url"

type Addr struct {
	*url.URL
}

func (addr *Addr) Network() string { return "websocket" }
