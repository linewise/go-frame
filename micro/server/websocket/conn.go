package websocket

import (
	"bufio"
	"crypto/tls"
	"io"
	"net"
	"net/http"
	"net/url"
	"sync"
	"time"
)

const (
	maxControlFramePayloadLength = 125

	closeStatusNormal            = 1000
	closeStatusGoingAway         = 1001
	closeStatusProtocolError     = 1002
	closeStatusUnsupportedData   = 1003
	closeStatusFrameTooLarge     = 1004
	closeStatusNoStatusRcvd      = 1005
	closeStatusAbnormalClosure   = 1006
	closeStatusBadMessageData    = 1007
	closeStatusPolicyViolation   = 1008
	closeStatusTooBigData        = 1009
	closeStatusExtensionMismatch = 1010

	ContinuationFrame = 0
	TextFrame         = 1
	BinaryFrame       = 2
	CloseFrame        = 8
	PingFrame         = 9
	PongFrame         = 10
	UnknownFrame      = 255
)

type (
	frameReader interface {
		io.Reader
		PayloadType() byte
		HeaderReader() io.Reader
		TrailerReader() io.Reader
		Len() int
	}
	frameWriter interface {
		io.WriteCloser
	}
	frameReaderFactory interface {
		NewFrameReader() (r frameReader, err error)
	}
	frameWriterFactory interface {
		NewFrameWriter(payloadType byte) (w frameWriter, err error)
	}
	frameHandler interface {
		HandleFrame(frame frameReader) (r frameReader, err error)
		WriteClose(status int) (err error)
	}

	ConnConfig struct {
		Location      *url.URL    // websocket服务端地址
		Origin        *url.URL    // websocket客户端地址
		Protocol      []string    // 协议
		Version       int         // 协议版本
		TlsConfig     *tls.Config // wss的配置
		Header        http.Header // Additional header fields to be sent in WebSocket opening handshake.
		Dialer        *net.Dialer // Dialer used when opening websocket connections.
		handshakeData map[string]string
	}

	Conn struct {
		config  *ConnConfig
		request *http.Request

		buf *bufio.ReadWriter
		rwc io.ReadWriteCloser

		rio sync.Mutex
		frameReaderFactory
		frameReader

		wio sync.Mutex
		frameWriterFactory

		frameHandler
		PayloadType        byte
		defaultCloseStatus int

		MaxPayloadBytes int
	}
)

func (c *Conn) Read(msg []byte) (n int, err error) {
	c.rio.Lock()
	defer c.rio.Unlock()

again:
	if c.frameReader == nil {
		frame, err := c.frameReaderFactory.NewFrameReader()
		if err != nil {
			return 0, err
		}
		c.frameReader, err = c.frameHandler.HandleFrame(frame)
		if err != nil {
			return 0, err
		}
		if c.frameReader == nil {
			goto again
		}
	}
	n, err = c.frameReader.Read(msg)
	if err == io.EOF {
		if trailer := c.frameReader.TrailerReader(); trailer != nil {
			io.Copy(io.Discard, trailer)
		}
		c.frameReader = nil
		goto again
	}
	return n, err
}

func (c *Conn) Write(msg []byte) (n int, err error) {
	c.wio.Lock()
	defer c.wio.Unlock()

	w, err := c.frameWriterFactory.NewFrameWriter(c.PayloadType)
	if err != nil {
		return 0, err
	}
	n, err = w.Write(msg)
	w.Close()
	return n, err
}

func (c *Conn) Close() error {
	err := c.frameHandler.WriteClose(c.defaultCloseStatus)
	err1 := c.rwc.Close()
	if err != nil {
		return err
	}
	return err1
}

func (c *Conn) IsClientConn() bool { return c.request == nil }
func (c *Conn) IsServerConn() bool { return c.request != nil }

func (c *Conn) LocalAddr() net.Addr {
	if c.IsClientConn() {
		return &Addr{c.config.Origin}
	}
	return &Addr{c.config.Location}
}

func (c *Conn) RemoteAddr() net.Addr {
	if c.IsClientConn() {
		return &Addr{c.config.Location}
	}
	return &Addr{c.config.Origin}
}

func (c *Conn) SetDeadline(t time.Time) error {
	if conn, ok := c.rwc.(net.Conn); ok {
		return conn.SetDeadline(t)
	}
	return ErrSetDeadline
}

func (c *Conn) SetReadDeadline(t time.Time) error {
	if conn, ok := c.rwc.(net.Conn); ok {
		return conn.SetReadDeadline(t)
	}
	return ErrSetDeadline
}

func (c *Conn) SetWriteDeadline(t time.Time) error {
	if conn, ok := c.rwc.(net.Conn); ok {
		return conn.SetWriteDeadline(t)
	}
	return ErrSetDeadline
}

func (c *Conn) Config() *ConnConfig { return c.config }

func (c *Conn) Request() *http.Request { return c.request }
