package websocket

import (
	"encoding/binary"
	"io"
)

type hybiFrameHandler struct {
	conn        *Conn
	payloadType byte
}

func (h *hybiFrameHandler) HandleFrame(frame frameReader) (frameReader, error) {
	if h.conn.IsServerConn() {
		// The client MUST mask all frames sent to the server.
		if frame.(*hybiFrameReader).header.MaskingKey == nil {
			h.WriteClose(closeStatusProtocolError)
			return nil, io.EOF
		}
	} else {
		// The server MUST NOT mask all frames.
		if frame.(*hybiFrameReader).header.MaskingKey != nil {
			h.WriteClose(closeStatusProtocolError)
			return nil, io.EOF
		}
	}
	if header := frame.HeaderReader(); header != nil {
		io.Copy(io.Discard, header)
	}
	switch frame.PayloadType() {
	case ContinuationFrame:
		frame.(*hybiFrameReader).header.OpCode = h.payloadType
	case TextFrame, BinaryFrame:
		h.payloadType = frame.PayloadType()
	case CloseFrame:
		return nil, io.EOF
	case PingFrame, PongFrame:
		b := make([]byte, maxControlFramePayloadLength)
		n, err := io.ReadFull(frame, b)
		if err != nil && err != io.EOF && err != io.ErrUnexpectedEOF {
			return nil, err
		}
		io.Copy(io.Discard, frame)
		if frame.PayloadType() == PingFrame {
			if _, err := h.WritePong(b[:n]); err != nil {
				return nil, err
			}
		}
		return nil, nil
	}
	return frame, nil
}

func (h *hybiFrameHandler) WriteClose(status int) (err error) {
	h.conn.wio.Lock()
	defer h.conn.wio.Unlock()
	w, err := h.conn.frameWriterFactory.NewFrameWriter(CloseFrame)
	if err != nil {
		return err
	}
	msg := make([]byte, 2)
	binary.BigEndian.PutUint16(msg, uint16(status))
	_, err = w.Write(msg)
	w.Close()
	return err
}

func (h *hybiFrameHandler) WritePong(msg []byte) (n int, err error) {
	h.conn.wio.Lock()
	defer h.conn.wio.Unlock()
	w, err := h.conn.frameWriterFactory.NewFrameWriter(PongFrame)
	if err != nil {
		return 0, err
	}
	n, err = w.Write(msg)
	w.Close()
	return n, err
}
