package websocket

import (
	"context"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/endpoint"
	"net/http"
)

type (
	DecodeRequestFunc  func(ctx context.Context, r *Conn) (req any, err error)
	EncodeResponseFunc func(ctx context.Context, w *Conn, res any) error
	ErrorEncoder       func(ctx context.Context, err error, w *Conn)

	Handler struct {
		DecodeRequest  DecodeRequestFunc
		EncodeResponse EncodeResponseFunc
		ErrorEncoder   ErrorEncoder
		Endpoint       endpoint.Endpoint
	}
)

// NewHandler 实例化处理
func NewHandler(decode DecodeRequestFunc, endpoint endpoint.Endpoint) *Handler {
	return &Handler{
		DecodeRequest:  decode,
		EncodeResponse: DefaultEncodeResponse,
		ErrorEncoder:   DefaultErrorEncoder,
		Endpoint:       endpoint,
	}
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// 接收http连接
	rwc, buf, err := w.(http.Hijacker).Hijack()
	if err != nil {
		logger.Logger.Panic("Hijack failed: " + err.Error())
	}

	// 创建websocket链接
	defer rwc.Close()
	config := ConnConfig{}
	conn, err := NewConn(rwc, buf, r, &config, CheckOrigin)
	if err != nil {
		return
	}
	if conn == nil {
		logger.Logger.Panic("unexpected nil conn")
	}

	// 接收和处理数据
	for {
		ctx := r.Context()

		// 接受解码数据
		req, err := h.DecodeRequest(ctx, conn)
		if err != nil {
			logger.Logger.Errorf("websocket接收数据失败：%v", err)
			break
		}

		// 业务处理
		res, err := h.Endpoint(ctx, req)
		if err != nil {
			h.ErrorEncoder(ctx, err, conn)
			break
		}

		// 返回数据
		if err := h.EncodeResponse(ctx, conn, res); err != nil {
			h.ErrorEncoder(ctx, err, conn)
			break
		}
	}
}
