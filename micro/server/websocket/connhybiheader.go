package websocket

import (
	"bytes"
)

type hybiFrameHeader struct {
	Fin        bool
	Rsv        [3]bool
	OpCode     byte
	Length     int64
	MaskingKey []byte
	data       *bytes.Buffer
}
