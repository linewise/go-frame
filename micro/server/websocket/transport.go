package websocket

import (
	"context"
	"net/http"
)

func DefaultEncodeResponse(ctx context.Context, writer *Conn, response any) error {
	ReturnErr := &Response{
		Success: true,
		Message: "",
		Code:    http.StatusOK,
		Data:    response,
	}
	return JSON.Send(writer, ReturnErr)
}

func DefaultErrorEncoder(ctx context.Context, err error, writer *Conn) {
	ReturnErr := &Response{
		Success: true,
		Message: err.Error(),
		Code:    http.StatusInternalServerError,
		Data:    "",
	}
	if err, ok := err.(*Response); ok {
		ReturnErr = err
	}

	JSON.Send(writer, ReturnErr)
}
