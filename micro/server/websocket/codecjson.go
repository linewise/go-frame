package websocket

import (
	"encoding/json"
)

var JSON = Codec{jsonMarshal, jsonUnmarshal}

func jsonMarshal(v any) (msg []byte, payloadType byte, err error) {
	msg, err = json.Marshal(v)
	return msg, TextFrame, err
}

func jsonUnmarshal(msg []byte, payloadType byte, v any) (err error) {
	return json.Unmarshal(msg, v)
}
