package websocket

import (
	"context"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/endpoint"
	"gitee.com/linewise/go-frame/micro/limiter"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
	"testing"
)

// endpoint
type TestEndpoints struct{}

func NewTestEndpoints() TestEndpoints {
	return TestEndpoints{}
}

func (l TestEndpoints) Test() endpoint.Endpoint {
	endpoint := func(ctx context.Context, request any) (any, error) {
		logger.Logger.Info("业务处理....")
		return struct {
			A string
		}{A: "aaa"}, nil
	}
	return endpoint
}

// transport
func NewTestWebsocketHandler(r *mux.Router, endpoints TestEndpoints) {
	r.Methods("GET").Path("/recvSend").Handler(NewHandler(DecodeTestRequest, endpoints.Test()))
	r.Methods("GET").Path("/web").HandlerFunc(web)
}

func web(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("index.html")
	t.Execute(w, nil)
}

func DecodeTestRequest(ctx context.Context, ws *Conn) (any, error) {
	var buf string
	err := Message.Receive(ws, &buf)
	if err != nil {
		return nil, err
	}
	logger.Logger.Info("接收到数据：", buf)
	return buf, err
}

// 启动服务
func TestServer_Run(t *testing.T) {
	handler := mux.NewRouter()
	NewTestWebsocketHandler(handler, NewTestEndpoints())

	limiter := limiter.NewLimiter(limiter.Config{Rate: 100})
	nacosServerConfig := []nacos.ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := nacos.ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := nacos.NewNacos(nacosServerConfig, nacosClientConfig)

	server := NewServer("test", "开发环境", "local", "test", ServerConfig{
		Host:         "127.0.0.1",
		Port:         "1234",
		ReadTimeout:  10,
		WriteTimeout: 10,
	}, handler, limiter, nacos)
	server.Run()
}
