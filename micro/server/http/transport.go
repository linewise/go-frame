package http

import (
	"context"
	"encoding/json"
	"net/http"
)

func DefaultEncodeResponse(ctx context.Context, writer http.ResponseWriter, response any) error {
	ReturnErr := &Response{
		Success: true,
		Message: "",
		Code:    http.StatusOK,
		Data:    response,
	}

	writer.Header().Set("Content-Type", "application/json;charset=utf-8")
	return json.NewEncoder(writer).Encode(ReturnErr)
}

func DefaultErrorEncoder(ctx context.Context, err error, writer http.ResponseWriter) {
	ReturnErr := &Response{
		Success: true,
		Message: err.Error(),
		Code:    http.StatusInternalServerError,
		Data:    "",
	}
	if err, ok := err.(*Response); ok {
		writer.WriteHeader(err.Code)
		ReturnErr = err
	} else {
		writer.WriteHeader(http.StatusInternalServerError)
	}

	writer.Header().Set("Content-Type", "application/json,charset=utf-8")
	json.NewEncoder(writer).Encode(ReturnErr)
}
