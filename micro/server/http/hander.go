package http

import (
	"context"
	"gitee.com/linewise/go-frame/micro/endpoint"
	"net/http"
)

type (
	DecodeRequestFunc  func(ctx context.Context, r *http.Request) (req any, err error)
	EncodeResponseFunc func(ctx context.Context, w http.ResponseWriter, res any) error
	ErrorEncoder       func(ctx context.Context, err error, w http.ResponseWriter)

	Handler struct {
		DecodeRequest  DecodeRequestFunc
		EncodeResponse EncodeResponseFunc
		ErrorEncoder   ErrorEncoder
		Endpoint       endpoint.Endpoint
	}
)

// NewHandler 实例化处理
func NewHandler(decode DecodeRequestFunc, endpoint endpoint.Endpoint) *Handler {
	return &Handler{
		DecodeRequest:  decode,
		EncodeResponse: DefaultEncodeResponse,
		ErrorEncoder:   DefaultErrorEncoder,
		Endpoint:       endpoint,
	}
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req, err := h.DecodeRequest(ctx, r)
	if err != nil {
		h.ErrorEncoder(ctx, err, w)
		return
	}

	res, err := h.Endpoint(ctx, req)
	if err != nil {
		h.ErrorEncoder(ctx, err, w)
		return
	}

	if err := h.EncodeResponse(ctx, w, res); err != nil {
		h.ErrorEncoder(ctx, err, w)
		return
	}
}
