package http

import (
	"context"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/endpoint"
	"gitee.com/linewise/go-frame/micro/limiter"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"github.com/gorilla/mux"
	"net/http"
	"testing"
)

// endpoint
type TestEndpoints struct{}

func NewTestEndpoints() TestEndpoints {
	return TestEndpoints{}
}

func (l TestEndpoints) Test() endpoint.Endpoint {
	endpoint := func(ctx context.Context, request any) (any, error) {
		return struct {
			A string
		}{A: "aaa"}, nil
	}
	return endpoint
}

// transport
func NewTestHttpHandler(r *mux.Router, endpoints TestEndpoints) {
	r.Methods("GET").Path("/test").Handler(NewHandler(DecodeLoginRequest, endpoints.Test()))
}

func DecodeLoginRequest(ctx context.Context, req *http.Request) (any, error) {
	return nil, nil
}

// 启动服务
func TestServer_Run(t *testing.T) {
	handler := mux.NewRouter()
	NewTestHttpHandler(handler, NewTestEndpoints())

	limiter := limiter.NewLimiter(limiter.Config{Rate: 100})
	nacosServerConfig := []nacos.ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := nacos.ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := nacos.NewNacos(nacosServerConfig, nacosClientConfig)

	server := NewServer("test", "开发环境", "local", "test", ServerConfig{
		Host:         "127.0.0.1",
		Port:         "1234",
		ReadTimeout:  10,
		WriteTimeout: 10,
	}, handler, limiter, nacos)

	server.Before(func(ctx context.Context, r *http.Request) context.Context {
		logger.Logger.Info("2222222")
		return ctx
	})

	server.After(func(ctx context.Context, r http.ResponseWriter) context.Context {
		logger.Logger.Info("3333")
		return ctx
	})

	logger.Logger.Info("444444")

	logger.Logger.Error(server.Run())
}
