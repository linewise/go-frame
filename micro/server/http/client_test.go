package http

import (
	"context"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"testing"
)

func TestClient_Do(t *testing.T) {
	nacosServerConfig := []nacos.ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		Scheme:      "http",
	}}
	nacosClientConfig := nacos.ClientConfig{
		NamespaceId:         "72a9f1f6-0325-44ac-867c-b4c0a70e6b19",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := nacos.NewNacos(nacosServerConfig, nacosClientConfig)

	client := NewClient(nacos)
	t.Log(client.Do(context.Background(), "test", "/test1", "GET", nil))
}
