package http

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"io"
	"net/http"
	"time"
)

type (
	ClientConfig struct {
		Retry   int64 `mapstructure:"retry"` // 重试次数
		Timeout int64 `mapstructure:"timeout"`
	}

	Client struct {
		Config ClientConfig
		Nacos  *nacos.Nacos
	}
)

func NewClient(n *nacos.Nacos) *Client {
	return &Client{Nacos: n}
}

func (c *Client) init() error {
	// 初始化配置文件
	if c.Config.Timeout == 0 {
		c.Config.Timeout = constants.DefaultTimeout
	}
	if c.Config.Retry == 0 {
		c.Config.Retry = constants.DefaultRetryCount
	}
	return nil
}

func (c *Client) Do(ctx context.Context, serverName string, path string, method string, req []byte) (any, error) {
	// 服务发现
	ip, port, err := c.Nacos.GetOneServer(serverName)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprint("http://", ip, ":", port, path)
	respData, err := c.Run(ctx, url, method, req)
	logger.Logger.Infof("请求链接：%s，返回数据：%s，err：%+v", url, respData, err)
	return respData, err
}

func (c *Client) Run(ctx context.Context, url string, method string, data []byte) ([]byte, error) {
	if err := c.init(); err != nil {
		return nil, err
	}

	var (
		client   = &http.Client{Timeout: time.Duration(c.Config.Timeout) * time.Second}
		payload  = bytes.NewReader(data)
		execFunc = func() (*http.Response, error) {
			req, err := http.NewRequest(method, url, payload)
			if err != nil {
				return nil, err
			}

			req.Header.Set("Content-Type", "application/json;charset=utf-8")
			var resp *http.Response
			for times := 1; times <= int(c.Config.Retry); times++ {
				resp, err = client.Do(req)
				if err == nil {
					return resp, nil
				}
				logger.Logger.Infof("%s第%d次请求，状态码：%d", url, times, resp.StatusCode)
			}

			return resp, err
		}
	)

	// 请求数据
	resp, err := execFunc()
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	// 解析数据
	bodyJsonData, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return bodyJsonData, nil
}
