package tcp

import (
	"context"
	"encoding/json"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"io"
	"net"
	"net/http"
	"time"
)

type Conn struct {
	server *Server
	rwc    net.Conn
}

func NewConn(c net.Conn, s *Server) *Conn {
	return &Conn{server: s, rwc: c}
}

func (c Conn) Run() {
	var (
		ctx     = context.Background()
		decoder = json.NewDecoder(c.rwc)
		encoder = json.NewEncoder(c.rwc)
		req     = Request{}
		res     = Response{
			Success: true,
			Code:    http.StatusOK,
			Message: "请求成功",
			Data:    nil,
		}

		// 接收数据函数
		receiveFunc = func(d *json.Decoder, req *Request) error {
			return d.Decode(req)
		}
		// 发送数据函数
		sendFunc = func(e *json.Encoder, res Response) error {
			return e.Encode(res)
		}
		// 调用处理数据函数
		handlerFunc = func(req Request) Response {
			var (
				res                       Response
				ctxTimeout, cancelTimeout = context.WithTimeout(ctx, time.Duration(c.server.Config.Timeout)*time.Second)
				endChan                   = make(chan Response)
			)
			defer cancelTimeout()

			go func() {
				res, err := c.server.Handler.Serve(ctxTimeout, req)
				if err != nil {
					res.Message = err.Error()
					res.Data = nil
				}
				endChan <- res
			}()

			select {
			case <-ctxTimeout.Done():
				res.Message = constants.ErrHandlerTimeout.Error()
				res.Code = http.StatusInternalServerError
			case res = <-endChan:
			}
			return res
		}
	)
	defer c.rwc.Close()

	for {
		// 接收数据
		err := receiveFunc(decoder, &req)
		if err == io.EOF {
			logger.Logger.Warnf("客户端连接已断开: %v", err)
			c.server.delConn(&c.rwc)
			break
		}
		if err != nil {
			logger.Logger.Errorf("从客户端读取数据失败: %v", err)
			sendFunc(encoder, DefaultErrorResponseMsg(constants.ErrDecodeParams.Error()))
			continue
		}

		// 处理数据
		res = handlerFunc(req)

		// 返回数据
		res.Id = req.Id
		if err = sendFunc(encoder, res); err != nil {
			logger.Logger.Errorf("返回客户端失败: %v", err)
			continue
		}
	}
}
