package tcp

import "testing"

func TestClient_Run(t *testing.T) {
	c := NewClient("127.0.0.1", "8080")
	c.Run()

	for i := 0; i < 100; i++ {
		t.Log(c.Do(Request{
			Method: "sss",
			Params: i,
		}))
	}
	c.Close()
}

func TestClient_Run1(t *testing.T) {
	c := NewClient("127.0.0.1", "8080")
	c.Run()

	for i := 0; i < 100; i++ {
		t.Log(c.Do(Request{
			Method: "22222",
			Params: i,
		}))
	}
	c.Close()
}
