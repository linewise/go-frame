package tcp

import "context"

type Handler interface {
	Serve(ctx context.Context, r Request) (w Response, err error)
}
