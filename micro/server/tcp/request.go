package tcp

type Request struct {
	Id     string `json:"id"`
	Method string `json:"method"`
	Params any    `json:"params"`
}
