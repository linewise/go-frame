package tcp

import (
	"encoding/json"
	"gitee.com/linewise/go-frame/micro/constants"
	"io"
	"net/http"
)

type Response struct {
	Id      string `json:"id"`
	Success bool   `json:"success"`
	Code    int    `json:"code"`
	Message string `json:"message,omitempty"`
	Data    any    `json:"datas,omitempty"`
}

// DefaultErrorConnMaxEncoder 链接数超了
func DefaultErrorConnMaxEncoder(writer io.Writer) error {
	encoder := json.NewEncoder(writer)
	return encoder.Encode(&Response{
		Success: true,
		Message: constants.ErrServerConnManyToMax.Error(),
		Code:    http.StatusInternalServerError,
		Data:    nil,
	})
}

// DefaultErrorResponseMsg 根据错误信息返回
func DefaultErrorResponseMsg(msg string) Response {
	return Response{
		Success: true,
		Code:    http.StatusInternalServerError,
		Message: msg,
		Data:    nil,
	}
}
