package tcp

import (
	"fmt"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/micro/limiter"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"net"
	"strconv"
	"sync"
)

type (
	ServerConfig struct {
		Host     string `mapstructure:"host"`
		Port     string `mapstructure:"port"`
		Timeout  int64  `mapstructure:"timeout"`
		MaxPool  int64  `mapstructure:"maxPool"`
		Metadata map[string]string
	}

	Server struct {
		Identity     string
		IdentityName string
		Environment  string
		Name         string
		Config       ServerConfig
		Handler      Handler
		Limiter      *limiter.Limiter
		Nacos        *nacos.Nacos

		mu          sync.Mutex
		activeConns map[*net.Conn]*Conn
	}
)

// NewServer tcp的服务端
func NewServer(identity, identityName, environment, name string, c ServerConfig, h Handler, l *limiter.Limiter, n *nacos.Nacos) *Server {
	return &Server{
		Identity:     identity,
		IdentityName: identityName,
		Environment:  environment,
		Name:         name,
		Config:       c,
		Handler:      h,
		Limiter:      l,
		Nacos:        n,
	}
}

func (s *Server) init() error {
	// 配置文件检测
	if s.Config.Host == "" || s.Config.Port == "" || s.Name == "" {
		return constants.ErrServerConfig
	}

	// 配置文件设置
	if s.Config.Timeout == 0 {
		s.Config.Timeout = constants.DefaultTimeout
	}
	if s.Config.MaxPool == 0 {
		s.Config.MaxPool = constants.DefaultServerMaxPool
	}
	s.Config.Metadata = map[string]string{"type": "tcp"}
	s.activeConns = make(map[*net.Conn]*Conn)
	return nil
}

// Run 运行服务
func (s *Server) Run() error {
	// 检测和初始化配置文件
	if err := s.init(); err != nil {
		return err
	}
	defer s.Close()

	// 监听端口
	addr, err := net.ResolveTCPAddr("tcp4", fmt.Sprintf("%s:%s", s.Config.Host, s.Config.Port))
	if err != nil {
		return err
	}
	listen, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return err
	}

	// 服务注册
	port, err := strconv.Atoi(s.Config.Port)
	if err != nil {
		return constants.ErrServerPort
	}
	success, err := s.Nacos.Register(nacos.RegisterConfig{
		Ip:          s.Config.Host,
		Port:        uint64(port),
		ServiceName: s.Name,
		GroupName:   s.IdentityName + "(" + s.Environment + ")",
		Weight:      10,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
		Metadata:    s.Config.Metadata,
	})
	logger.Logger.Infof("服务注册：%v, err: %v", success, err)

	// 接收连接
	for {
		// 限流
		s.Limiter.Limit()

		// 接收连接
		conn, err := listen.Accept()
		if err != nil {
			continue
		}

		// 链接数超了
		if int64(len(s.activeConns)) >= s.Config.MaxPool {
			DefaultErrorConnMaxEncoder(conn)
			continue
		}

		// 处理
		myConn := NewConn(conn, s)
		s.activeConns[&conn] = myConn
		go myConn.Run()
	}
}

func (s *Server) delConn(c *net.Conn) {
	if _, ok := s.activeConns[c]; ok {
		delete(s.activeConns, c)
	}
}

// Close 关闭服务和所有连接
func (s *Server) Close() {
	s.mu.Lock()
	defer s.mu.Unlock()

	for c := range s.activeConns {
		(*c).Close()
		delete(s.activeConns, c)
	}
}
