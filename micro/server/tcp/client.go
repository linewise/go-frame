package tcp

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/linewise/go-frame/logger"
	"gitee.com/linewise/go-frame/micro/constants"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"github.com/pborman/uuid"
	"io"
	"net"
	"net/http"
	"time"
)

type (
	ClientConfig struct {
		ServerName string `mapstructure:"serverName"`
		Host       string `mapstructure:"host"`
		Port       string `mapstructure:"port"`
		Timeout    int64  `mapstructure:"timeout"`
	}

	Client struct {
		Config ClientConfig
		Nacos  *nacos.Nacos

		conn        net.Conn
		channel     chan Request
		requestList map[string]chan Response
		pool        int64
	}
)

// NewClient tcp的客户端
func NewClient(host, port string) *Client {
	return &Client{
		Config: ClientConfig{
			Host: host,
			Port: port,
		},
	}
}

// NewClientByServerName tcp的客户端
func NewClientByServerName(n *nacos.Nacos, serverName string) *Client {
	return &Client{
		Config: ClientConfig{
			ServerName: serverName,
		},
		Nacos: n,
	}
}

func (c *Client) init() error {
	// 配置文件检测
	if c.Config.ServerName == "" && c.Nacos == nil {
		// 服务发现
		ip, port, err := c.Nacos.GetOneServer(c.Config.ServerName)
		if err != nil {
			return err
		}
		c.Config.Host = ip
		c.Config.Port = fmt.Sprint(port)
	}
	if c.Config.Host == "" || c.Config.Port == "" {
		return constants.ErrServerConfig
	}

	// 初始化配置文件
	if c.Config.Timeout == 0 {
		c.Config.Timeout = constants.DefaultTimeout
	}
	c.pool = constants.DefaultClientPool
	c.channel = make(chan Request, c.pool)
	c.requestList = make(map[string]chan Response, 0)
	return nil
}

// Run 运行
func (c *Client) Run() error {
	// 初始化配置文件
	c.init()

	var (
		addr = fmt.Sprintf("%s:%s", c.Config.Host, c.Config.Port)

		// 接收数据
		receiveFunc = func() {
			in := json.NewDecoder(c.conn)
			for {
				var (
					res Response
					err = in.Decode(&res)
				)
				if err == io.EOF {
					logger.Logger.Warnf("连接断开: %v", err)
					c.Close()
					break
				}
				if err != nil {
					logger.Logger.Errorf("接收失败: %s", err)
					continue
				}

				id := res.Id
				if resChan, ok := c.requestList[id]; ok != true {
					logger.Logger.Errorf("返回数据id不存在: %s", id)
				} else {
					resChan <- res
				}
			}
		}

		// 发送数据
		sendFunc = func() {
			for {
				req := <-c.channel
				logger.Logger.Infof("发送数据：%v", req)
				b, err := json.Marshal(req)
				if err != nil {
					logger.Logger.Errorf("tcp1 client send json err: %v", err)
					continue
				}
				c.conn.Write(b)
			}
		}
	)

	// 建立连接
	conn, err := net.DialTimeout("tcp", addr, time.Duration(c.Config.Timeout)*time.Second)
	if err != nil {
		logger.Logger.Errorf("dial failed err: %v", err)
		return err
	}
	c.conn = conn

	// 发送接收数据
	go sendFunc()
	go receiveFunc()
	return nil
}

// Close 断开连接
func (c *Client) Close() {
	c.conn.Close()
}

// Do 请求运行
func (c *Client) Do(req Request) (res Response, err error) {
	var (
		uuid        = uuid.New()
		ctx, cancel = context.WithTimeout(context.Background(), time.Duration(c.Config.Timeout)*time.Second)
	)
	defer cancel()

	// 发送数据
	req.Id = uuid
	c.requestList[req.Id] = make(chan Response)
	c.channel <- req

	// 接收数据
	select {
	case <-ctx.Done():
		res.Success = false
		res.Code = http.StatusInternalServerError
		res.Message = constants.ErrHandlerTimeout.Error()
	case res = <-c.requestList[req.Id]:
	}
	return
}
