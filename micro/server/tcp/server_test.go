package tcp

import (
	"context"
	"gitee.com/linewise/go-frame/micro/limiter"
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"log"
	"testing"
)

type DemoHandler struct{}

func (d DemoHandler) Serve(ctx context.Context, req Request) (res Response, err error) {
	log.Println(req)
	return Response{
		Success: true,
		Code:    200,
		Message: "demo",
		Data:    req.Params,
	}, nil
}

func TestServer_Run(t *testing.T) {
	limiter := limiter.NewLimiter(limiter.Config{Rate: 100})
	nacosServerConfig := []nacos.ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		GrpcPort:    9848,
		Scheme:      "http",
	}}
	nacosClientConfig := nacos.ClientConfig{
		Username:            "nacos",
		Password:            "nacos",
		NamespaceId:         "691eb202-449d-486c-af13-755beaaefa04",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := nacos.NewNacos(nacosServerConfig, nacosClientConfig)

	server := NewServer("test", "开发环境", "local", "test", ServerConfig{
		Host:    "127.0.0.1",
		Port:    "8080",
		Timeout: 10,
	}, DemoHandler{}, limiter, nacos)

	t.Log(server.Run())
}
