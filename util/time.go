package util

import (
	"time"
)

type Time struct {
	Format string
}

// NowTime 当前时间
func (t Time) NowTime() string {
	return time.Now().Format(t.Format)
}

// ToString 时间返回字符串
func (t Time) ToString(ti time.Time) string {
	return ti.Format(t.Format)
}

// StringToTime 字符串转换成时间
func (t Time) StringToTime(s string) (rt time.Time, err error) {
	return time.Parse(t.Format, s)
}

// ParseInLocation 字符串转换成时间
func (t Time) ParseInLocation(s string) (time.Time, error) {
	return time.ParseInLocation(t.Format, s, time.Local)
}

// NowZero 当天凌晨
func (t Time) NowZero() string {
	now := time.Now()
	return time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location()).Format(t.Format)
}

// Tomorrow 明天
func (t Time) Tomorrow() string {
	return time.Now().AddDate(0, 0, 1).Format(t.Format)
}

// TomorrowZeroString 明天凌晨
func (t Time) TomorrowZeroString() string {
	tomorrow := time.Now().AddDate(0, 0, 1)
	return time.Date(tomorrow.Year(), tomorrow.Month(), tomorrow.Day(), 0, 0, 0, 0, tomorrow.Location()).Format(t.Format)
}

// TomorrowZero 明天凌晨
func (t Time) TomorrowZero() time.Time {
	tomorrow := time.Now().AddDate(0, 0, 1)
	return time.Date(tomorrow.Year(), tomorrow.Month(), tomorrow.Day(), 0, 0, 0, 0, tomorrow.Location())
}
