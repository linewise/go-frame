package util

import (
	"testing"
	"time"
)

func TestNewSyncChanUtil(t *testing.T) {
	s := NewSyncChanUtil(3) // 最多并发数3

	// 循环10次，并发数最多为3
	for i := 0; i < 10; i++ {
		s.Add()
		go func(i int) {
			defer s.Done()

			t.Log(i)
			time.Sleep(time.Second)
		}(i)
	}
	s.Wait()
}
