package util

import (
	"regexp"
	"strings"
)

type Regex struct{}

// IsPhone 判断手机号
func (t Regex) IsPhone(s string) bool {
	reg := regexp.MustCompile(`^1[3456789]\d{9}$`)
	return reg.MatchString(s)
}

// IsEmail 判断邮箱
func (t Regex) IsEmail(s string) bool {
	reg := regexp.MustCompile(`^\w+@([\da-z\.-]+)\.([a-z\x{4e00}-\x{9fa5}]+)$`)
	return reg.MatchString(s)
}

// Password 密码长度8为以上数字和字母
func (t Regex) Password(s string) bool {
	if len(s) > 15 || len(s) < 8 {
		return false
	}
	reg := regexp.MustCompile(`(^(?:.*[A-Za-z].*)(?:.*[0-9].*))|(^(?:.*[0-9].*)(?:.*[A-Za-z].*))`)
	return reg.MatchString(s)
}

// Camel2Case 驼峰转小写下划线
// AbbCdd转换成abb_cdd
func (t Regex) Camel2Case(s string) string {
	reg := regexp.MustCompile(`(\w)([A-Z])`)
	return strings.ToLower(reg.ReplaceAllString(s, `${1}_${2}`))
}
