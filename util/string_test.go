package util

import "testing"

func TestString_StructToString(t *testing.T) {
	a := struct {
		Name  string `json:"name"`
		Phone string `json:"phone"`
		Sex   string `json:"sex,omitempty"`
	}{
		Name:  "111",
		Phone: "222",
		Sex:   "333",
	}

	t.Log(NewString("&").JoinByStruct(a))
	t.Log("2021-08-10" > "2022-08-09")
}
