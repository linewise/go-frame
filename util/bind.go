package util

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

type Bind struct {
	IgnoreEmpty bool // 是否忽视空值
	DeepCopy    bool // 是否深度拷贝
}

// CopyJson 将源数据src值赋值给目标数据dst src->dst
// 通过json的方式赋值
func (b Bind) CopyJson(dst, src any) error {
	dstPtrType := reflect.TypeOf(dst)
	if dstPtrType.Kind() != reflect.Ptr || dstPtrType.Elem().Kind() == reflect.Ptr {
		return errors.New("传入目标参数必须是指针类型的值")
	}
	srcBytes, err := json.Marshal(src)

	if err != nil {
		return err
	}

	err = json.Unmarshal(srcBytes, dst)
	return err
}

// Copy 将源数据src值赋值给目标数据dst src->dst
func (b Bind) Copy(dst, src any) (err error) {
	srcValue := b.reflectValue(reflect.ValueOf(src))
	dstValue := b.reflectValue(reflect.ValueOf(dst))
	if !dstValue.CanAddr() {
		return errors.New("目标值无效，不能被赋值")
	}
	if !srcValue.IsValid() {
		return errors.New("源数据无效")
	}

	srcType, srcIsPtr := b.reflectType(reflect.TypeOf(src))
	dstType, _ := b.reflectType(reflect.TypeOf(dst))
	if srcType.Kind() == reflect.Interface {
		srcType = reflect.TypeOf(srcValue.Interface())
	}
	if dstType.Kind() == reflect.Interface {
		dstType, _ = b.reflectType(reflect.TypeOf(dstValue.Interface()))
		oldDst := dstValue
		dstValue = reflect.New(reflect.TypeOf(dstValue.Interface())).Elem()
		defer func() {
			oldDst.Set(dstValue)
		}()
	}

	// 如果是一个一般类型
	if srcValue.Kind() != reflect.Slice && srcValue.Kind() != reflect.Struct && srcValue.Kind() != reflect.Map && (srcValue.Type().AssignableTo(dstValue.Type()) || srcValue.Type().ConvertibleTo(dstValue.Type())) {
		if !srcIsPtr || !b.DeepCopy {
			dstValue.Set(srcValue.Convert(dstValue.Type()))
		} else {
			srcCopy := reflect.New(srcValue.Type())
			srcCopy.Set(srcValue.Elem())
			dstValue.Set(srcCopy.Convert(dstValue.Type()))
		}
		return err
	}

	// 如果是map类型
	if srcValue.Kind() != reflect.Slice && srcType.Kind() == reflect.Map && dstType.Kind() == reflect.Map {
		if !srcType.Key().ConvertibleTo(dstType.Key()) {
			return errors.New("MAP的KEY类型不匹配")
		}

		if dstValue.IsNil() {
			dstValue.Set(reflect.MakeMapWithSize(dstType, srcValue.Len()))
		}

		for _, k := range srcValue.MapKeys() {
			dstKey := b.reflectValue(reflect.New(dstType.Key()))
			if !b.set(dstKey, k) {
				return fmt.Errorf("不支持MAP赋值，源值类型 %v，目标值%v", k.Type(), dstType.Key())
			}

			elemType, _ := b.reflectType(dstType.Elem())
			dstV := b.reflectValue(reflect.New(elemType))
			if !b.set(dstV, srcValue.MapIndex(k)) {
				if err = b.Copy(dstV.Addr().Interface(), srcValue.MapIndex(k).Interface()); err != nil {
					return err
				}
			}

			for {
				if elemType == dstType.Elem() {
					dstValue.SetMapIndex(dstKey, dstV)
					break
				}

				elemType = reflect.PtrTo(elemType)
				dstV = dstV.Addr()
			}
		}
		return err
	}

	// 如果是slice类型
	if srcValue.Kind() == reflect.Slice && dstValue.Kind() == reflect.Slice && srcType.ConvertibleTo(dstType) {
		if dstValue.IsNil() {
			dstValue.Set(reflect.MakeSlice(reflect.SliceOf(dstValue.Type().Elem()), srcValue.Len(), srcValue.Cap()))
		}

		for i := 0; i < srcValue.Len(); i++ {
			if dstValue.Len() < i+1 {
				dstValue.Set(reflect.Append(dstValue, reflect.New(dstValue.Type().Elem()).Elem()))
			}

			if !b.set(dstValue.Index(i), srcValue.Index(i)) {
				if err = b.Copy(dstValue.Index(i).Addr().Interface(), srcValue.Index(i).Interface()); err != nil {
					continue
				}
			}
		}
		return err
	}

	// 跳过不支持的类型
	if srcType.Kind() != reflect.Struct || dstType.Kind() != reflect.Struct {
		return err
	}

	// struct类型
	isSlice := false
	amount := 1
	if srcValue.Kind() == reflect.Slice || dstValue.Kind() == reflect.Slice {
		isSlice = true
		if srcValue.Kind() == reflect.Slice {
			amount = srcValue.Len()
		}
	}
	for i := 0; i < amount; i++ {
		var srcTemp, dstTemp reflect.Value
		if isSlice {
			if srcValue.Kind() == reflect.Slice {
				srcTemp = b.reflectValue(srcValue.Index(i))
			} else {
				srcTemp = b.reflectValue(srcValue)
			}
			dstTemp = b.reflectValue(reflect.New(dstType).Elem())
		} else {
			srcTemp = b.reflectValue(srcValue)
			dstTemp = b.reflectValue(dstValue)
		}

		dstTempIsInterface := false
		if dstTemp.Kind() == reflect.Interface {
			dstTempIsInterface = true
			dstTemp = b.reflectValue(reflect.New(dstType))
		}

		if srcTemp.IsValid() {
			srcTypeFields := b.deepFields(srcType)
			for _, field := range srcTypeFields {
				name := field.Name
				if srcField := srcTemp.FieldByName(name); srcField.IsValid() && !b.shouldIgnore(srcField) {
					dstFieldNotSet := false

					if f, ok := dstTemp.Type().FieldByName(name); ok {
						for idx := range f.Index {
							dstField := dstTemp.FieldByIndex(f.Index[:idx+1])
							if dstField.Kind() != reflect.Ptr {
								continue
							}
							if !dstField.IsNil() {
								continue
							}
							if !dstField.CanSet() {
								dstFieldNotSet = true
								break
							}
							dstField.Set(reflect.New(dstField.Type().Elem()))
						}
					}

					if dstFieldNotSet {
						break
					}

					dstField := dstTemp.FieldByName(name)
					if dstField.IsValid() {
						if dstField.CanSet() {
							if !b.set(dstField, srcField) {
								if err := b.Copy(dstField.Addr().Interface(), srcField.Interface()); err != nil {
									return err
								}
							}
						}
					} else {
						var toMethod reflect.Value
						if dstTemp.CanAddr() {
							toMethod = dstTemp.Addr().MethodByName(name)
						} else {
							toMethod = dstTemp.MethodByName(name)
						}

						if toMethod.IsValid() && toMethod.Type().NumIn() == 1 && srcField.Type().AssignableTo(toMethod.Type().In(0)) {
							toMethod.Call([]reflect.Value{srcField})
						}
					}
				}
			}

			for _, field := range b.deepFields(dstType) {
				name := field.Name
				var fromMethod reflect.Value
				if srcTemp.CanAddr() {
					fromMethod = srcTemp.Addr().MethodByName(name)
				} else {
					fromMethod = srcTemp.MethodByName(name)
				}

				if fromMethod.IsValid() && fromMethod.Type().NumIn() == 0 && fromMethod.Type().NumOut() == 1 && !b.shouldIgnore(fromMethod) {
					if toField := dstTemp.FieldByName(name); toField.IsValid() && toField.CanSet() {
						values := fromMethod.Call([]reflect.Value{})
						if len(values) >= 1 {
							b.set(toField, values[0])
						}
					}
				}
			}
		}

		if isSlice && dstValue.Kind() == reflect.Slice {
			if dstTemp.Addr().Type().AssignableTo(dstValue.Type().Elem()) {
				if dstValue.Len() < i+1 {
					dstValue.Set(reflect.Append(dstValue, dstTemp.Addr()))
				} else {
					if !b.set(dstValue.Index(i), dstTemp.Addr()) {
						err = b.Copy(dstValue.Index(i).Addr().Interface(), dstTemp.Addr().Interface())
						if err != nil {
							continue
						}
					}
				}
			} else if dstTemp.Type().AssignableTo(dstValue.Type().Elem()) {
				if dstValue.Len() < i+1 {
					dstValue.Set(reflect.Append(dstValue, dstTemp))
				} else {
					if !b.set(dstValue.Index(i), dstTemp) {
						err = b.Copy(dstValue.Index(i).Addr().Interface(), dstTemp.Interface())
						if err != nil {
							continue
						}
					}
				}
			}
		} else if dstTempIsInterface {
			dstValue.Set(dstTemp)
		}
	}

	return err
}

func (b Bind) reflectValue(v reflect.Value) reflect.Value {
	for v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	return v
}

func (b Bind) reflectType(t reflect.Type) (_ reflect.Type, isPtr bool) {
	for t.Kind() == reflect.Ptr || t.Kind() == reflect.Slice {
		t = t.Elem()
		isPtr = true
	}

	return t, isPtr
}

func (b Bind) set(dstValue, srcValue reflect.Value) bool {
	if srcValue.IsValid() {
		if dstValue.Kind() == reflect.Ptr {
			if srcValue.Kind() == reflect.Ptr && srcValue.IsNil() {
				dstValue.Set(reflect.Zero(dstValue.Type()))
				return true
			} else if dstValue.IsNil() {
				if srcValuer, ok := b.driveValuer(srcValue); ok {
					v, err := srcValuer.Value()
					if err != nil {
						return false
					}
					if v == nil {
						return true
					}
				}
				dstValue.Set(reflect.New(dstValue.Type().Elem()))
			}

			dstValue = dstValue.Elem()
		}

		if b.DeepCopy {
			dstKind := dstValue.Kind()
			if dstKind == reflect.Interface && dstValue.IsNil() && reflect.TypeOf(srcValue.Interface()) != nil {
				dstValue.Set(reflect.New(reflect.TypeOf(srcValue.Interface())).Elem())
				dstKind = reflect.TypeOf(dstValue.Interface()).Kind()
			}

			if dstKind == reflect.Struct || dstKind == reflect.Map || dstKind == reflect.Slice {
				return false
			}
		}

		if srcValue.Type().ConvertibleTo(dstValue.Type()) {
			dstValue.Set(srcValue.Convert(dstValue.Type()))
		} else if dstScanner, ok := dstValue.Addr().Interface().(sql.Scanner); ok {
			if srcValue.Kind() == reflect.Ptr {
				if srcValue.IsNil() {
					return true
				}
				srcValue = b.reflectValue(srcValue)
			}

			if dstScanner.Scan(srcValue.Interface()) != nil {
				return false
			}
		} else if srcValuer, ok := b.driveValuer(srcValue); ok {
			v, err := srcValuer.Value()
			if err != nil {
				return false
			}
			if v == nil {
				return true
			}

			rv := reflect.ValueOf(v)
			if rv.Type().AssignableTo(dstValue.Type()) {
				dstValue.Set(rv)
			}
		} else if srcValue.Kind() == reflect.Ptr {
			return b.set(dstValue, srcValue.Elem())
		} else {
			return false
		}
	}

	return true
}

func (b Bind) driveValuer(v reflect.Value) (i driver.Valuer, ok bool) {
	if !v.CanAddr() {
		i, ok = v.Interface().(driver.Valuer)
		return
	}

	i, ok = v.Addr().Interface().(driver.Valuer)
	return
}

func (b Bind) deepFields(t reflect.Type) []reflect.StructField {
	if t, _ = b.reflectType(t); t.Kind() == reflect.Struct {
		fields := make([]reflect.StructField, 0, t.NumField())
		for i := 0; i < t.NumField(); i++ {
			v := t.Field(i)
			if v.Anonymous {
				fields = append(fields, b.deepFields(v.Type)...)
			} else {
				fields = append(fields, v)
			}
		}
		return fields
	}
	return nil
}

// 是否忽略空值
func (b Bind) shouldIgnore(v reflect.Value) bool {
	if !b.IgnoreEmpty {
		return false
	}

	return v.IsZero()
}
