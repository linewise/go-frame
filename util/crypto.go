package util

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"golang.org/x/crypto/bcrypt"
)

type Crypto struct {
	AesKey16 string // 加解密的密钥（长度为16位）
}

// Hash hash加密
func (c *Crypto) Hash(pwd string) (string, error) {
	if len(pwd) == 0 {
		return "", nil
	}
	r, err := bcrypt.GenerateFromPassword([]byte(pwd), 12)
	return string(r), err
}

// Check hash校验
func (c *Crypto) Check(pwd, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pwd))
	return err == nil
}

// Encrypt aes加密
func (c *Crypto) Encrypt(str string) (string, error) {
	aesKey16 := []byte(c.AesKey16)
	encryptStr := []byte(str)
	block, err := aes.NewCipher(aesKey16)
	if err != nil {
		return "", err
	}

	blockSize := block.BlockSize()
	padding := blockSize - len(encryptStr)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	encryptStr = append(encryptStr, padText...)
	blockMode := cipher.NewCBCEncrypter(block, aesKey16[:blockSize])
	encrypt := make([]byte, len(encryptStr))
	blockMode.CryptBlocks(encrypt, encryptStr)
	return base64.StdEncoding.EncodeToString(encrypt), err
}

// Decrypt aes解密
func (c *Crypto) Decrypt(encrypt string) (string, error) {
	aesKey16 := []byte(c.AesKey16)
	bytesPass, err := base64.StdEncoding.DecodeString(encrypt)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(aesKey16)
	if err != nil {
		return "", err
	}

	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, aesKey16[:blockSize])
	origData := make([]byte, len(bytesPass))
	blockMode.CryptBlocks(origData, bytesPass)
	length := len(origData)
	unPadding := int(origData[length-1])
	origData = origData[:(length - unPadding)]
	return string(origData[:]), err
}
