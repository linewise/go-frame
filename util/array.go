package util

type Array[T comparable] struct{}

// Exist 数组中是否存在某值，返回下标
func (a Array[T]) Exist(array []T, val T) (index int) {
	index = -1
	for i, t := range array {
		if t == val {
			return i
		}
	}

	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// Unique 数组去重
func (a Array[T]) Unique(array ...T) []T {
	arrayMap := make(map[T]string)
	for _, s := range array {
		arrayMap[s] = ""
	}

	var unique []T
	for key, _ := range arrayMap {
		unique = append(unique, key)
	}
	return unique
}
