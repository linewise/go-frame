package util

import "testing"

func TestTimeUtil_NowTime(t *testing.T) {
	t.Log(Time{Format: "2006-01-02 15:04:05"}.NowTime())
	t.Log(Time{Format: "2006-01-02 15:04:05"}.Tomorrow())
	t.Log(Time{Format: "2006-01-02 15:04:05"}.TomorrowZero())
	t.Log(Time{Format: "2006-01-02"}.StringToTime("2021-10-14"))
}
