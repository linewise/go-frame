package util

import "testing"

func TestArray_Exist(t *testing.T) {
	type Name string
	var arrayInterface = Array[Name]{}
	t.Log(arrayInterface.Exist([]Name{"1", "2"}, "2"))
	t.Log(arrayInterface.Exist([]Name{"1", "2"}, Name("2")))

	t.Log(Array[string]{}.Exist([]string{"1", "2"}, "2"))
}
