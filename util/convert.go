package util

import (
	"reflect"
	"strings"
)

type Convert struct{}

// StructToMap 结构体转map
func (c Convert) StructToMap(obj any) map[string]any {
	obj1 := reflect.TypeOf(obj)
	obj2 := reflect.ValueOf(obj)
	var data = make(map[string]any)
	for i := 0; i < obj1.NumField(); i++ {
		tagNames := strings.Split(obj1.Field(i).Tag.Get("json"), ",")
		tagName := obj1.Field(i).Name
		if len(tagNames) > 0 && tagNames[0] != "" {
			tagName = tagNames[0]
		}
		omitemptyIndex := Array[string]{}.Exist(tagNames, "omitempty")
		if !(omitemptyIndex != -1 && obj2.Field(i).Interface() == "") {
			data[tagName] = obj2.Field(i).Interface()
		}
	}
	return data
}
