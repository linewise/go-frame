package util

import "github.com/mozillazg/go-pinyin"

type PinYin struct{}

// PinYin 中文汉字全拼，且拼音首字母大写
func (p PinYin) PinYin(str string) (retStr string) {
	rule := pinyin.NewArgs()
	for _, value := range pinyin.Pinyin(str, rule) {
		for _, valueV := range value {
			retStr += Capitalize(valueV)
		}
	}
	return retStr
}

// PinYinFirst 中文汉字拼音首字母小写
func (p PinYin) PinYinFirst(str string) (retStr string) {
	rule := pinyin.NewArgs()
	for _, value := range pinyin.Pinyin(str, rule) {
		for _, valueV := range value {
			vv := []rune(valueV) // 后文有介绍
			retStr += string(vv[0])
		}
	}
	return retStr
}

// Capitalize 字符首字母大写转换
func Capitalize(str string) string {
	var upperStr string
	vv := []rune(str)
	for i := 0; i < len(vv); i++ {
		if i == 0 {
			if vv[i] >= 97 && vv[i] <= 122 {
				vv[i] -= 32 // string的码表相差32位
				upperStr += string(vv[i])
			} else {
				return str
			}
		} else {
			upperStr += string(vv[i])
		}
	}
	return upperStr
}
