package util

import (
	"github.com/gookit/validate"
	"regexp"
	"strings"
	"testing"
)

func TestRegexUtil_IsPhone(t *testing.T) {
	regex := Regex{}
	t.Log(regex.IsPhone("18200250600"))
	t.Log(regex.IsPhone("18780160736"))
}

func TestRegexUtil_IsEmail(t *testing.T) {
	regex := Regex{}
	t.Log(regex.IsEmail("lsj@qq.com"))
	t.Log(regex.IsEmail("wang@yihuan.com"))

	type User struct {
		Phone  string `json:"phone" bson:"phone" validate:"required|len:11"` // 手机号
		Emails string `json:"emails" bson:"emails" validate:"email"`
	}
	v := validate.Struct(User{
		Phone:  "18780160736",
		Emails: "12@qq.com",
	})
	if isValid := v.Validate(); !isValid {
		t.Log(v.Errors)
	}
}

func TestIsLocalIP(t *testing.T) {
	reg := regexp.MustCompile(`^192|10.*`)
	t.Log(reg.MatchString("192.168.9.3"))
	t.Log(reg.MatchString("10.168.9.3"))
	t.Log(reg.MatchString("120.168.9.3"))
	t.Log(reg.MatchString("20.168.9.3"))
}

func TestCamel2Case(t *testing.T) {
	reg := regexp.MustCompile(`(\w)([A-Z])`)
	t.Log(strings.ToLower(reg.ReplaceAllString("AbbCdd", `${1}_${2}`)))
}
