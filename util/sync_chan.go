package util

import "sync"

type SyncChan struct {
	sg *sync.WaitGroup
	c  chan bool
}

// NewSyncChanUtil 多线程控制，限制线程数量
func NewSyncChanUtil(count int64) *SyncChan {
	s := &SyncChan{
		c:  make(chan bool, count),
		sg: &sync.WaitGroup{},
	}
	return s
}

// Add 实现多线程运行，且限制线程数量为num
func (s SyncChan) Add() {
	s.sg.Add(1)
	s.c <- true
}

// Wait 等待线程执行
func (s SyncChan) Wait() {
	s.sg.Wait()
}

// Done 线程运行结束是需要执行结束
func (s SyncChan) Done() {
	<-s.c
	s.sg.Done()
}
