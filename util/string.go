package util

import (
	"fmt"
	"reflect"
	"strings"
)

type String struct {
	split string
}

func NewString(split string) String {
	return String{split: split}
}

// JoinByStruct 对象转成字符串
func (s String) JoinByStruct(obj any) (str string, err error) {
	dataType := reflect.TypeOf(obj)
	dataValue := reflect.ValueOf(obj)
	for i := 0; i < dataType.NumField(); i++ {
		tagNames := strings.Split(dataType.Field(i).Tag.Get("json"), ",")
		tagName := dataType.Field(i).Name
		if len(tagNames) > 0 && tagNames[0] != "" {
			tagName = tagNames[0]
		}

		omitemptyIndex := Array[string]{}.Exist(tagNames, "omitempty")
		if !(omitemptyIndex != -1 && dataValue.Field(i).Interface() == "") {
			if str != "" {
				str = str + s.split
			}
			str = str + fmt.Sprint(tagName, "=", dataValue.Field(i).Interface())
		}
	}

	return
}
