package sms

import (
	"gitee.com/linewise/go-frame/logger"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	sms "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/sms/v20210111"
)

type (
	Config struct {
		SecretId    string `mapstructure:"secretId"`    // 秘钥id
		SecretKey   string `mapstructure:"secretKey"`   // 秘钥
		SmsSdkAppId string `mapstructure:"smsSdkAppId"` // 短信应用id
		SignName    string `mapstructure:"signName"`    // 短信签名内容
	}

	// TencentSms 腾讯云短信
	TencentSms struct {
		Config Config
		Client *sms.Client
	}
)

// NewTencentSms 实例化腾讯短信
// 短信控制台: https://console.cloud.tencent.com/sms/smslist
// sms helper: https://cloud.tencent.com/document/product/382/3773
// cpf.HttpProfile.ReqTimeout=5，设置超时时间，默认的超时时间为5秒
// cpf.HttpProfile.ReqMethod="POST"，默认POST，可换成GET方法
// cpf.HttpProfile.Endpoint="sms.tencentcloudapi.com"，SDK会自动指定域名。通常是不需要特地指定域名的，但是如果你访问的是金融区的服务则必须手动指定域名，例如sms的上海金融区域名： sms.ap-shanghai-fsi.tencentcloudapi.com
// cpf.SignMethod="HmacSHA1"，SDK默认用TC3-HMAC-SHA256进行签名，非必要请不要修改这个字段
// 在github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/regions中可查找到sms.NewClient的地域信息
func NewTencentSms(c Config) *TencentSms {
	credential := common.NewCredential(c.SecretId, c.SecretKey) // 实例化一个认证对象
	cpf := profile.NewClientProfile()                           // 实例化一个客户端配置对象

	// 实例化client对象
	// 第二个参数是地域信息，可以直接填写字符串ap-guangzhou或者引用预设的常量
	client, err := sms.NewClient(credential, "ap-chengdu", cpf)
	if err != nil {
		logger.Logger.Error(err)
		return nil
	}

	return &TencentSms{
		Config: c,
		Client: client,
	}
}

// SendSms 发送短信
// 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录[短信控制台]查看
// 国际/港澳台短信SenderId: 国内短信填空，默认未开通，如需开通请联系 [sms helper]
// SessionContext：用户的session内容，可以携带用户侧ID等上下文信息，server会原样返回
// ExtendCode：短信码号扩展号
// TemplateParamSet：模板参数
// TemplateId：模板ID，必须填写已审核通过的模板ID
func (t *TencentSms) SendSms(phoneNumberSet []string, templateId string, templateParamSet []string) {
	request := sms.NewSendSmsRequest()
	request.SmsSdkAppId = common.StringPtr(t.Config.SmsSdkAppId)
	request.SignName = common.StringPtr(t.Config.SignName)
	request.SenderId = common.StringPtr("")
	request.SessionContext = common.StringPtr("")                  // 用户的session内容
	request.ExtendCode = common.StringPtr("")                      // 短信码号扩展号
	request.TemplateParamSet = common.StringPtrs(templateParamSet) // 模板参数
	request.TemplateId = common.StringPtr(templateId)              // 模板ID

	// 每次发送手机号最多不要超过200个手机号
	phoneLength := len(phoneNumberSet)
	i := 0
	for {
		returnFlag := false
		if i+200 <= phoneLength {
			request.PhoneNumberSet = common.StringPtrs(phoneNumberSet[i : i+200])
		} else {
			request.PhoneNumberSet = common.StringPtrs(phoneNumberSet[i:])
			returnFlag = true
		}

		// 下发手机号码，采用 e.164 标准，+[国家或地区码][手机号]
		// 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号
		response, err := t.Client.SendSms(request)
		if err != nil {
			logger.Logger.Errorf("tencentSms send An API error has returned: %s", err)
			return
		}
		logger.Logger.Info(response.Response)

		if returnFlag {
			break
		}
		i = i + 200
	}
}
