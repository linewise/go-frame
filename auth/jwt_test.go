package auth

import (
	"testing"
)

func TestNewJwt(t *testing.T) {
	token := NewJwt("123", 0)
	jwt, err := token.Create()
	t.Log(err)
	t.Log(jwt)

	v, er := token.Parse(jwt)
	t.Log(er)
	t.Log(v)
}
