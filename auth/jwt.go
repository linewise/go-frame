package auth

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type Jwt struct {
	Secret []byte // 密钥
	Expire int64  // 有效期（分钟）
	jwt.StandardClaims
	//jwt.StandardClaims
	//Audience  string `json:"aud,omitempty"` // 受众群体
	//ExpiresAt int64  `json:"exp,omitempty"` // 到期时间
	//Id        string `json:"jti,omitempty"` // 编号
	//IssuedAt  int64  `json:"iat,omitempty"` // 签发时间
	//Issuer    string `json:"iss,omitempty"` // 签发人
	//NotBefore int64  `json:"nbf,omitempty"` // 生效时间
	//Subject   string `json:"sub,omitempty"` // 主题
}

// NewJwt 有效期时长默认2小时
func NewJwt(secret string, expire int64) *Jwt {
	if expire <= 0 {
		expire = 2 * 60
	}

	return &Jwt{
		Secret: []byte(secret),
		Expire: expire,
	}
}

// Create 生成JWT
func (t Jwt) Create() (string, error) {
	t.StandardClaims = jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Duration(t.Expire) * time.Minute).Unix(), // 到期时间
		IssuedAt:  time.Now().Unix(),                                            // 签发时间
		NotBefore: time.Now().Unix(),                                            // 生效时间
	}

	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, t)
	return tokenClaims.SignedString(t.Secret)
}

// Parse 解析JWT
func (t Jwt) Parse(strToken string) (jwt.MapClaims, error) {
	jwtToken, err := jwt.ParseWithClaims(strToken, jwt.MapClaims{}, func(token *jwt.Token) (i any, err error) {
		return t.Secret, nil
	})
	if err != nil || jwtToken == nil {
		return nil, err
	}

	if err = jwtToken.Claims.Valid(); err != nil {
		return nil, err
	}

	claim, ok := jwtToken.Claims.(jwt.MapClaims)
	if ok && jwtToken.Valid {
		return claim, nil
	}

	return nil, nil
}

// Check 校验是否有效
func (t Jwt) Check(strToken string) (bool, error) {
	jwtToken, err := jwt.ParseWithClaims(strToken, jwt.MapClaims{}, func(token *jwt.Token) (i any, err error) {
		return t.Secret, nil
	})
	if err != nil || jwtToken == nil {
		return false, err
	}

	if err = jwtToken.Claims.Valid(); err != nil {
		return false, err
	}

	return true, nil
}

// GetAudience 返回受众群体
func (t Jwt) GetAudience() string {
	return t.Audience
}
