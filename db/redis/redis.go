package redis

import (
	"context"
	"gitee.com/linewise/go-frame/logger"
	"github.com/go-redis/redis/v8"
	"time"
)

type Config struct {
	Addr     string `mapstructure:"addr"`
	Password string `mapstructure:"password"`
	DbIndex  int    `mapstructure:"dbIndex"`
	Timeout  int    `mapstructure:"timeout"`
	PoolSize int    `mapstructure:"poolSize"`
}

// NewRedis 始化redis
func NewRedis(c Config) *redis.Client {
	if c.Addr == "" {
		return nil
	}

	client := redis.NewClient(&redis.Options{
		Addr:         c.Addr,
		Password:     c.Password,
		DB:           c.DbIndex,
		PoolSize:     c.PoolSize,
		PoolTimeout:  time.Second * time.Duration(c.Timeout),
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
		DialTimeout:  time.Second * 10,
	})

	ctx := context.Background()
	if err := client.Ping(ctx).Err(); err != nil {
		logger.Logger.Panic("Connect to redis error:", err)
	}

	logger.Logger.Infof("%s, Redis连接成功...", c.Addr)
	return client
}
