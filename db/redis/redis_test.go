package redis

import (
	"context"
	"testing"
	"time"
)

func TestNewRedis(t *testing.T) {
	config := Config{
		Addr:     "127.0.0.1:6379",
		Password: "ZwpeCySh1mxcTGfu",
		DbIndex:  0,
		PoolSize: 20,
		Timeout:  30,
	}
	client := NewRedis(config)

	//string
	t.Log("string:")
	//err := client-get.Set(context.Background(), "test:str", "test111", time.Hour).Err()
	//t.Log(err)
	var str string
	err := client.Get(context.Background(), "test:str").Scan(&str)
	t.Log(err)
	t.Log(str)

	//hash
	t.Log("hash:")
	err = client.HSet(context.Background(), "test:hash", map[string]any{
		"Name":  "王龙军",
		"Phone": "18780160735",
	}).Err()
	t.Log(err)
	err = client.Expire(context.Background(), "test:hash", time.Hour).Err()
	t.Log(err)
	v, err := client.HGet(context.Background(), "test:hash", "Name").Result()
	t.Log(err)
	t.Log(v)
	vs, err := client.HVals(context.Background(), "test:hash").Result()
	t.Log(err)
	t.Log(vs)
}

func TestNewRedisL(t *testing.T) {
	config := Config{
		Addr:     "192.168.4.9:6379",
		Password: "",
		DbIndex:  0,
		PoolSize: 20,
		Timeout:  30,
	}
	client := NewRedis(config)

	total, err := client.LLen(context.Background(), "ss").Result()
	t.Log(err)
	t.Log(total)
}
