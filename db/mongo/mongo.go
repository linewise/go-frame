package mongo

import (
	"context"
	"errors"
	"gitee.com/linewise/go-frame/logger"
	"go.mongodb.org/mongo-driver/event"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
	"strings"
	"time"
)

type (
	Config struct {
		Conn               string `json:"conn" mapstructure:"conn"`
		CtxTimeout         int64  `mapstructure:"ctxTimeout"`
		SetLocalThreshold  int    `mapstructure:"setLocalThreshold"`
		SetMaxConnIdleTime int    `mapstructure:"setMaxConnIdleTime"`
		SetMaxPoolSize     uint64 `mapstructure:"setMaxPoolSize"`
	}

	Mongo struct {
		Config       Config
		Client       *mongo.Client
		Database     *mongo.Database
		IndexOptions *options.CreateIndexesOptions
		Collections  map[string]*mongo.Collection
	}
)

// NewMongo 连接mongo服务
func NewMongo(c Config) (*Mongo, error) {
	if c.CtxTimeout == 0 {
		c.CtxTimeout = 60
	}
	if c.SetLocalThreshold == 0 {
		c.SetLocalThreshold = 5
	}
	if c.SetMaxConnIdleTime == 0 {
		c.SetMaxConnIdleTime = 10
	}
	if c.SetMaxPoolSize == 0 {
		c.SetMaxPoolSize = 200
	}
	if c.Conn == "" {
		return nil, errors.New("mongo链接字符串为空")
	}

	// 实例化mongo
	drive := &Mongo{Config: c}
	if err := drive.conn(); err != nil {
		return drive, err
	}

	return drive, nil
}

// conn 创建mongo连接
func (m *Mongo) conn() error {
	// 获取连接中数据库名称
	connString, err := connstring.ParseAndValidate(m.Config.Conn)
	if err != nil {
		return err
	}

	// readPreference 主要控制客户端 Driver 从复制集的哪个节点读取数据，这个特性可方便的实现读写分离、就近读取等策略
	// 1、primary 只从 primary 节点读数据，这个是默认设置
	// 2、primaryPreferred 优先从 primary 读取，primary 不可服务，从 secondary 读
	// 3、secondary 只从 secondary 节点读数据
	// 4、secondaryPreferred 优先从 secondary 读取，没有 secondary 成员时，从 primary 读取
	// 5、nearest 根据网络距离就近读取
	want, err := readpref.New(readpref.SecondaryMode)
	if err != nil {
		logger.Logger.Panicf("使用mongo辅助节点错误：%s", err)
	}
	wc := writeconcern.New(writeconcern.WMajority())

	// 事件监听
	comMonitor := &event.CommandMonitor{
		Failed: func(ctx context.Context, failedEvent *event.CommandFailedEvent) {
			logger.Logger.Error("Connect to mongodb err.")
		},
	}

	// 连接到MongoDB
	ctx, _ := m.GetCtx()
	clientOptions := options.Client().ApplyURI(m.Config.Conn)
	clientOptions.SetLocalThreshold(time.Duration(m.Config.SetLocalThreshold) * time.Second)   // 只使用与mongo操作耗时小于X秒
	clientOptions.SetMaxConnIdleTime(time.Duration(m.Config.SetMaxConnIdleTime) * time.Second) // 指定连接可以保持空闲的最大X秒
	clientOptions.SetMaxPoolSize(m.Config.SetMaxPoolSize)                                      // 使用最大的连接数
	clientOptions.SetReadPreference(want)                                                      // 表示只使用辅助节点
	clientOptions.SetReadConcern(readconcern.Majority())                                       // 指定查询应返回实例的最新数据确认为，已写入副本集中的大多数成员
	clientOptions.SetWriteConcern(wc)                                                          // 请求确认写操作传播到大多数mongodb实例
	clientOptions.SetMonitor(comMonitor)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		logger.Logger.Panic("Connect to mongodb err:", err)
	}

	// 检查连接
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		logger.Logger.Panic("Ping to mongodb err:", err)
	}
	logger.Logger.Infof("%s, Mongodb连接成功...", m.Config.Conn)

	// 初始化数据
	m.Client = client
	m.Database = m.Client.Database(connString.Database)
	m.IndexOptions = options.CreateIndexes().SetMaxTime(time.Duration(m.Config.CtxTimeout) * time.Second)
	m.Collections = make(map[string]*mongo.Collection)
	return nil
}

// GetCtx 返回ctx和ctxCancel
func (m *Mongo) GetCtx() (ctx context.Context, ctxCancel context.CancelFunc) {
	ctx, ctxCancel = context.WithTimeout(context.Background(), time.Duration(m.Config.CtxTimeout)*time.Second)
	return
}

// Register 添加Collection
func (m *Mongo) Register(name string) (err error) {
	m.Collections[name] = m.Database.Collection(name)
	return
}

// CreateIndex 创建索引
func (m *Mongo) CreateIndex(collection string, unique bool, keys ...string) error {
	db := m.Collections[collection]
	if db == nil {
		return errors.New("collection is disabled")
	}

	indexView := db.Indexes()
	keysDoc := bsonx.Doc{}

	// 复合索引
	for _, key := range keys {
		if strings.HasPrefix(key, "-") {
			keysDoc = keysDoc.Append(strings.TrimLeft(key, "-"), bsonx.Int32(-1))
		} else {
			keysDoc = keysDoc.Append(key, bsonx.Int32(1))
		}
	}

	// 创建索引
	result, err := indexView.CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    keysDoc,
			Options: options.Index().SetUnique(unique),
		},
		m.IndexOptions,
	)

	if result == "" {
		return errors.New("created collection error")
	}

	return err
}
