package kafka

import (
	"fmt"
	"gitee.com/linewise/go-frame/logger"
	"github.com/Shopify/sarama"
	"time"
)

type Config struct {
	Addrs []string
	Topic string
}

func NewSyncProducer(c Config) (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll          // 等待服务器所有副本都保存成功后的响应
	config.Producer.Partitioner = sarama.NewRandomPartitioner // 随机向partition发送消息
	config.Producer.Return.Successes = true                   // 是否等待成功和失败后的响应，只有上面的RequireAcks设置不是NoReponse这里才有用
	config.Producer.Return.Errors = true
	config.Producer.Retry.Max = 100
	config.Producer.Timeout = 5 * time.Second
	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	config.Version = sarama.V2_8_0_0                         // 设置使用的kafka版本，如果低于V0_10_0_0版本，消息中的timestrap没有作用。需要消费和生产同时配置（注意，版本设置不对的话，kafka会返回很奇怪的错误，并且无法成功发送消息）

	// 使用配置，新建一个异步生产者
	producer, err := sarama.NewSyncProducer(c.Addrs, config)
	defer producer.Close()

	return producer, err
}

// NewAsyncProducer 创建一个异步生产者
func NewAsyncProducer(c Config) (sarama.AsyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll          // 等待服务器所有副本都保存成功后的响应
	config.Producer.Partitioner = sarama.NewRandomPartitioner // 随机向partition发送消息
	config.Producer.Return.Successes = true                   // 是否等待成功和失败后的响应，只有上面的RequireAcks设置不是NoReponse这里才有用
	config.Producer.Return.Errors = true
	config.Producer.Retry.Max = 100
	config.Producer.Timeout = 5 * time.Second
	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	config.Version = sarama.V2_8_0_0                         // 设置使用的kafka版本，如果低于V0_10_0_0版本，消息中的timestrap没有作用。需要消费和生产同时配置（注意，版本设置不对的话，kafka会返回很奇怪的错误，并且无法成功发送消息）

	// 使用配置，新建一个异步生产者
	producer, err := sarama.NewAsyncProducer(c.Addrs, config)
	if err != nil {
		return producer, err
	}
	// defer producer.Close()

	// 循环判断哪个通道发送过来数据
	go func(p sarama.AsyncProducer) {
		for {
			select {
			case suc := <-p.Successes():
				fmt.Println("发送成功：offset: ", suc.Offset, "; timestamp: ", suc.Timestamp.String(), "; partitions: ", suc.Partition)
			case fail := <-p.Errors():
				fmt.Println("err: ", fail.Err)
			}
		}
	}(producer)

	return producer, err
}

// NewConsumer 创建一个消费者
func NewConsumer(c Config) error {
	config := sarama.NewConfig()
	config.Consumer.Offsets.AutoCommit.Interval = 1 * time.Second // 提交offset的间隔时间，每秒提交一次给kafka
	config.Version = sarama.V2_8_0_0                              // 设置使用的kafka版本，如果低于V0_10_0_0版本，消息中的timestrap没有作用。需要消费和生产同时配置

	// consumer新建的时候会新建一个client，这个client归属于这个consumer，并且这个client不能用作其他的consumer
	consumer, err := sarama.NewConsumer(c.Addrs, config)
	if err != nil {
		return err
	}
	defer consumer.Close()

	partitionConsumer, err := consumer.ConsumePartition(c.Topic, 0, sarama.OffsetOldest)
	if err != nil {
		return err
	}
	defer partitionConsumer.Close()

	for msg := range partitionConsumer.Messages() {
		logger.Logger.Infof("Partition:%d Offset:%d Key:%v Value:%v", msg.Partition, msg.Offset, msg.Key, string(msg.Value))
	}

	//Trap SIGINT to trigger a shutdown.
	//	signals := make(chan os.Signal, 1)
	//	signal.Notify(signals, os.Interrupt)
	//
	//ConsumerLoop:
	//	for {
	//		select {
	//		case msg1 := <-partitionConsumer.Messages():
	//			log.Printf("Consumed message offset %d\n", msg1.Offset)
	//			log.Println(string(msg1.Value))
	//		case <-signals:
	//			break ConsumerLoop
	//		}
	//	}

	return nil
}
