package kafka

import (
	"github.com/Shopify/sarama"
	"testing"
	"time"
)

func TestNewAsyncProducer(t *testing.T) {
	producer, err := NewAsyncProducer(Config{
		Addrs: []string{"127.0.0.1:9092"},
		Topic: "0606_test",
	})

	if err != nil {
		t.Error(err)
	}

	//发送数据
	var value string
	for i := 0; ; i++ {
		time.Sleep(2 * time.Second)

		//发送的消息，主题。
		//注意：这里的msg必须得是新构建的变量，不然你会发现发送过去的消息内容都是一样的，因为批次发送消息的关系。
		value = "消息生产者：发送消息 " + time.Now().Format("15:04:05")
		t.Log(value)
		msg := &sarama.ProducerMessage{
			Topic: "0606_test",
			Value: sarama.ByteEncoder(value), //将字符串转化为字节数组
		}

		//使用通道发送
		producer.Input() <- msg
	}
}

func TestNewConsumer(t *testing.T) {
	t.Log(NewConsumer(Config{
		Addrs: []string{"127.0.0.1:9092"},
		Topic: "0606_test",
	}))
}
