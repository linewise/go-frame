package rabbitmq

import (
	"encoding/json"
	"time"
)

type Msg struct {
	Data   any // 发送的内容
	Method string
	Time   time.Time
}

func (m Msg) String() []byte {
	if m.Time.IsZero() {
		m.Time = time.Now()
	}

	if data, err := json.Marshal(m); err == nil {
		return data
	}
	return nil
}

func (m Msg) Msg(data []byte) Msg {
	if json.Unmarshal(data, &m) != nil {
		return Msg{}
	}
	return m
}
