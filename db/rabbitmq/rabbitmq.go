package rabbitmq

import (
	"gitee.com/linewise/go-frame/logger"
	"github.com/streadway/amqp"
)

type (
	Config struct {
		Addr string `mapstructure:"addr"`
	}

	Rabbitmq struct {
		Conn    *amqp.Connection
		Channel *amqp.Channel
	}
)

func NewRabbitmq(c Config) *Rabbitmq {
	if c.Addr == "" {
		return nil
	}

	client := &Rabbitmq{}
	if err := client.Connection(c); err != nil {
		logger.Logger.Panicf("rabbitmq链接信息获取失败：%s", err)
	}

	return client
}

func (r *Rabbitmq) Connection(c Config) error {
	var err error

	if r.Conn, err = amqp.Dial(c.Addr); err == nil {
		r.Channel, err = r.Conn.Channel()
	}
	return err
}

func (r *Rabbitmq) Close() {
	r.Channel.Close()
	r.Conn.Close()
}
