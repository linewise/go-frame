package mysql

import (
	"fmt"
	"gitee.com/linewise/go-frame/logger"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"time"
)

type Config struct {
	Host     string `mapstructure:"host"`
	Port     uint16 `mapstructure:"port"`
	Database string `mapstructure:"database"`
	UserName string `mapstructure:"userName"`
	Password string `mapstructure:"password"`
	LogMode  int    `mapstructure:"logMode"`

	TablePrefix        string `mapstructure:"tablePrefix"`
	MaxIdleConnections int    `mapstructure:"maxIdleConnections"`
	MaxOpenConnections int    `mapstructure:"maxOpenConnections"`
}

// NewMysql 初始化mysql
func NewMysql(c Config) (client *gorm.DB) {
	if c.Host == "" {
		return nil
	}
	if c.LogMode == 0 {
		c.LogMode = 4
	}

	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%v)/%s?charset=utf8mb4&parseTime=True&loc=Local", c.UserName, c.Password, c.Host, c.Port, c.Database)
	client, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: c.TablePrefix,
		},
		Logger: gormLogger.Default.LogMode(gormLogger.LogLevel(c.LogMode)),
		NowFunc: func() time.Time {
			return time.Now().Local()
		},
		QueryFields: true, //会根据字段自动筛选
	})
	if err != nil {
		logger.Logger.Panicf("连接%s数据库失败：%s", c.Database, err)
	}

	sqlDB, err := client.DB()
	if err != nil {
		logger.Logger.Panicf("连接%s数据库失败：%s", c.Database, err)
	}
	logger.Logger.Info("mysql数据库连接成功")
	sqlDB.SetMaxIdleConns(c.MaxIdleConnections) // SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxOpenConns(c.MaxOpenConnections) // SetMaxOpenConns 设置打开数据库连接的最大数量
	sqlDB.SetConnMaxLifetime(time.Minute)       // SetConnMaxLifetime 设置了连接可复用的最大时间

	return
}
