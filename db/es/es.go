package es

import (
	"encoding/json"
	"gitee.com/linewise/go-frame/logger"
	"github.com/elastic/elastic-transport-go/v8/elastictransport"
	elasticsearchV7 "github.com/elastic/go-elasticsearch/v7"
	elasticsearchV8 "github.com/elastic/go-elasticsearch/v8"
	"os"
)

type (
	Config struct {
		Addrs []string `mapstructure:"addrs"`
	}

	SearchResponse struct {
		Took     uint64 `json:"took"`
		TimedOut bool   `json:"timed_out"`
		Shards   struct {
			Total      uint64 `json:"total"`
			Successful uint64 `json:"successful"`
			Skipped    uint64 `json:"skipped"`
			Failed     uint64 `json:"failed"`
		} `json:"_shards"`
		Hits         *SearchResponseHits        `json:"hits"`
		Aggregations map[string]json.RawMessage `json:"aggregations"`
	}

	SearchResponseHits struct {
		Total struct {
			Value    uint64 `json:"value"`
			Relation string `json:"relation"`
		} `json:"total"`
		MaxScore float64                   `json:"max_score"`
		Hits     []*SearchResponseHitsHits `json:"hits"`
	}

	SearchResponseHitsHits struct {
		Index  string          `json:"_index"`
		Type   string          `json:"_type"`
		ID     string          `json:"_id"`
		Score  float64         `json:"_score"`
		Source json.RawMessage `json:"_source"`
	}
)

func NewEsV8(c Config) *elasticsearchV8.Client {
	if len(c.Addrs) <= 0 {
		return nil
	}

	config := elasticsearchV8.Config{
		Addresses: c.Addrs,
		Logger:    &elastictransport.ColorLogger{Output: os.Stdout}, // {Output: os.Stdout, EnableRequestBody: true, EnableResponseBody: true},
	}

	client, err := elasticsearchV8.NewClient(config)
	if err != nil {
		logger.Logger.Panicf("es链接失败：%s", err)
	}

	resp, err := client.Info()
	if err != nil {
		logger.Logger.Panicf("es链接信息获取失败：%s", err)
	}
	logger.Logger.Info(resp)

	return client
}

func NewEsV7(c Config) *elasticsearchV7.Client {
	if len(c.Addrs) <= 0 {
		return nil
	}

	config := elasticsearchV7.Config{
		Addresses: c.Addrs,
		//Logger:    &estransport.ColorLogger{Output: os.Stdout, EnableRequestBody: true, EnableResponseBody: true},
	}

	client, err := elasticsearchV7.NewClient(config)
	if err != nil {
		logger.Logger.Panicf("es链接失败：%s", err)
	}

	resp, err := client.Info()
	if err != nil {
		logger.Logger.Panicf("es链接信息获取失败：%s", err)
	}
	logger.Logger.Info(resp)

	return client
}
