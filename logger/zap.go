package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
	"path/filepath"
	"sync"
)

var (
	sp                             = string(filepath.Separator)
	errWS, warnWS, infoWS, debugWS zapcore.WriteSyncer       // IO输出
	debugConsoleWS                 = zapcore.Lock(os.Stdout) // 控制台标准输出
	errorConsoleWS                 = zapcore.Lock(os.Stderr)
	levelMap                       = map[string]zapcore.Level{
		"debug": zapcore.DebugLevel,
		"info":  zapcore.InfoLevel,
		"warn":  zapcore.WarnLevel,
		"error": zapcore.ErrorLevel,
	}
)

type (
	ZapConfig struct {
		AppName       string // 日志文件前缀
		LogFileDir    string // 文件保存地方
		ErrorFileName string
		WarnFileName  string
		InfoFileName  string
		DebugFileName string
		Level         zapcore.Level // 日志等级
		MaxSize       int           // 日志文件小大（M）
		MaxBackups    int           // 最多存在多少个切片文件
		MaxAge        int           // 保存的最大天数
		Development   bool          // 是否是开发模式
		zap.Config
	}

	ZapLogger struct {
		*zap.Logger
		sync.RWMutex
		Config    *ZapConfig
		zapConfig zap.Config
	}
)

// NewZapLogger 实例化日志
func NewZapLogger(mod ...ModConfig) *ZapLogger {
	var (
		fileDir, _ = filepath.Abs(filepath.Dir(filepath.Join(".")))
		logFileDir = fileDir + sp + "logs" + sp
		l          = &ZapLogger{
			Config: &ZapConfig{
				LogFileDir:    logFileDir,
				AppName:       "app",
				ErrorFileName: "error",
				WarnFileName:  "warn",
				InfoFileName:  "info",
				DebugFileName: "debug",
				Development:   true,
				Level:         zapcore.DebugLevel,
				MaxSize:       100, // 每个日志文件保存100M，默认100M
				MaxBackups:    60,  // 保留60个备份，默认不限
				MaxAge:        180, // 保留180天，默认不限
			},
		}
	)

	l.Lock()
	defer l.Unlock()
	for _, fn := range mod {
		fn(l.Config)
	}
	if l.Config.LogFileDir == "" {
		l.Config.LogFileDir = logFileDir
	}
	if l.Config.Development {
		l.zapConfig = zap.NewDevelopmentConfig()
		l.zapConfig.EncoderConfig.EncodeTime = timeEncoder
	} else {
		l.zapConfig = zap.NewProductionConfig()
		l.zapConfig.EncoderConfig.EncodeTime = timeUnixNano
	}
	l.zapConfig.EncoderConfig.TimeKey = "T"
	l.zapConfig.EncoderConfig.LevelKey = "L"
	l.zapConfig.EncoderConfig.NameKey = "N"
	l.zapConfig.EncoderConfig.CallerKey = "C"
	l.zapConfig.EncoderConfig.MessageKey = "M"
	l.zapConfig.EncoderConfig.StacktraceKey = "S"
	if l.Config.OutputPaths == nil || len(l.Config.OutputPaths) == 0 {
		l.zapConfig.OutputPaths = []string{"stdout"}
	}
	if l.Config.ErrorOutputPaths == nil || len(l.Config.ErrorOutputPaths) == 0 {
		l.zapConfig.OutputPaths = []string{"stderr"}
	}
	l.zapConfig.Level.SetLevel(l.Config.Level)
	l.init()
	l.Info("[NewZapLogger] success")
	return l
}

func (z *ZapLogger) init() {
	z.setSyncers()

	var err error
	z.Logger, err = z.zapConfig.Build(z.cores())
	if err != nil {
		panic(err)
	}
	defer z.Logger.Sync()
}

func (z *ZapLogger) setSyncers() {
	f := func(fN string) zapcore.WriteSyncer {
		return zapcore.AddSync(&lumberjack.Logger{
			Filename:   z.Config.LogFileDir + sp + z.Config.AppName + "-" + fN + ".log", // z.Config.LogFileDir + sp + z.Config.AppName + "-" + fN + "-" + time.Now().Format("2006-01-02") + ".log",
			MaxSize:    z.Config.MaxSize,
			MaxBackups: z.Config.MaxBackups,
			MaxAge:     z.Config.MaxAge,
			Compress:   true,
			LocalTime:  true,
		})
	}
	errWS = f(z.Config.ErrorFileName)
	warnWS = f(z.Config.WarnFileName)
	infoWS = f(z.Config.InfoFileName)
	debugWS = f(z.Config.DebugFileName)
	return
}

func (z *ZapLogger) cores() zap.Option {
	fileEncoder := zapcore.NewJSONEncoder(z.zapConfig.EncoderConfig)
	encoderConfig := zap.NewDevelopmentEncoderConfig()
	encoderConfig.EncodeTime = timeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	consoleEncoder := zapcore.NewConsoleEncoder(encoderConfig)

	errPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl == zapcore.ErrorLevel && zapcore.ErrorLevel-z.zapConfig.Level.Level() > -1
	})
	warnPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl == zapcore.WarnLevel && zapcore.WarnLevel-z.zapConfig.Level.Level() > -1
	})
	infoPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl == zapcore.InfoLevel && zapcore.InfoLevel-z.zapConfig.Level.Level() > -1
	})
	debugPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl == zapcore.DebugLevel && zapcore.DebugLevel-z.zapConfig.Level.Level() > -1
	})

	cores := []zapcore.Core{
		zapcore.NewCore(fileEncoder, errWS, errPriority),
		zapcore.NewCore(fileEncoder, warnWS, warnPriority),
		zapcore.NewCore(fileEncoder, infoWS, infoPriority),
		zapcore.NewCore(fileEncoder, debugWS, debugPriority),
	}
	if z.Config.Development {
		cores = append(cores, []zapcore.Core{
			zapcore.NewCore(consoleEncoder, errorConsoleWS, errPriority),
			zapcore.NewCore(consoleEncoder, debugConsoleWS, warnPriority),
			zapcore.NewCore(consoleEncoder, debugConsoleWS, infoPriority),
			zapcore.NewCore(consoleEncoder, debugConsoleWS, debugPriority),
		}...)
	}
	return zap.WrapCore(func(c zapcore.Core) zapcore.Core {
		return zapcore.NewTee(cores...)
	})
}

func (z *ZapLogger) GetConfig() *ZapConfig {
	return z.Config
}
