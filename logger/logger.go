package logger

import "go.uber.org/zap"

var (
	ZapLog *ZapLogger
	Logger *zap.SugaredLogger
)

func init() {
	ZapLog = NewZapLogger()
	Logger = ZapLog.Sugar()
}

type Config struct {
	Name          string
	Level         string
	IsDevelopment bool
}

// InitLogger
// 设置日志级别
// debug 可以打印出 info debug warn
// info  级别可以打印 warn info
// warn  只能打印 warn
// debug->info->warn->error
func InitLogger(c Config) {
	ZapLog = NewZapLogger(
		SetAppName(c.Name),
		SetDevelopment(c.IsDevelopment),
		SetLevel(getLogLevel(c.Level)),
	)

	Logger = ZapLog.Sugar()
	Logger.Info("实例化日志完成。")
}

func Exit() {
	Logger.Sync() // 将buffer中的日志写到文件中
}
