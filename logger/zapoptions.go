package logger

import "go.uber.org/zap/zapcore"

type ModConfig func(*ZapConfig)

func SetMaxSize(MaxSize int) ModConfig {
	return func(config *ZapConfig) {
		config.MaxSize = MaxSize
	}
}

func SetMaxBackups(MaxBackups int) ModConfig {
	return func(config *ZapConfig) {
		config.MaxBackups = MaxBackups
	}
}

func SetMaxAge(MaxAge int) ModConfig {
	return func(config *ZapConfig) {
		config.MaxAge = MaxAge
	}
}

func SetLogFileDir(LogFileDir string) ModConfig {
	return func(config *ZapConfig) {
		config.LogFileDir = LogFileDir
	}
}

func SetAppName(AppName string) ModConfig {
	return func(config *ZapConfig) {
		config.AppName = AppName
	}
}

func SetLevel(Level zapcore.Level) ModConfig {
	return func(config *ZapConfig) {
		config.Level = Level
	}
}

func SetErrorFileName(ErrorFileName string) ModConfig {
	return func(config *ZapConfig) {
		config.ErrorFileName = ErrorFileName
	}
}

func SetWarnFileName(WarnFileName string) ModConfig {
	return func(config *ZapConfig) {
		config.WarnFileName = WarnFileName
	}
}

func SetInfoFileName(InfoFileName string) ModConfig {
	return func(config *ZapConfig) {
		config.InfoFileName = InfoFileName
	}
}

func SetDebugFileName(DebugFileName string) ModConfig {
	return func(config *ZapConfig) {
		config.DebugFileName = DebugFileName
	}
}

func SetDevelopment(Development bool) ModConfig {
	return func(config *ZapConfig) {
		config.Development = Development
	}
}
