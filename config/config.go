package config

import (
	"errors"
	"flag"
	"gitee.com/linewise/go-frame/logger"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"os"
	"reflect"
	"strings"
)

var (
	configPath  string
	configName  string
	environment string
)

// 初始化配置
// 接收命令行参数，否者默认
func init() {
	flag.StringVar(&configPath, "p", "./config/", " set logic config file path")
	flag.StringVar(&configName, "f", "config", " set logic config file name")
	flag.StringVar(&environment, "e", "local", " set logic config file name")
}

type Config struct {
	*viper.Viper
	Name        string   `json:"name"`        // 配置文件名称
	Paths       []string `json:"paths"`       // 配置文件路径
	Environment string   `json:"environment"` // 环境变量
}

// NewConfig 实例化配置
// 根据paths路径获取name的配置文件
// 默认name是config，paths为当前目录和当前路径下的config目录
func NewConfig(name string, paths ...string) *Config {
	config := &Config{
		Viper:       viper.New(),
		Name:        configName,
		Paths:       []string{"./", configPath},
		Environment: environment,
	}

	// 获取环境变量
	if os.Getenv("Env") != "" {
		config.Environment = os.Getenv("Env")
	}

	// 获取配置名称
	if name != "" {
		config.Name = name
	}

	// 配置文件路径
	if len(paths) != 0 {
		config.Paths = append(paths, config.Paths...)
	}
	for _, path := range config.Paths {
		config.AddConfigPath(path)
	}

	return config
}

// NewDefaultConfig 实例化配置
// 默认配置文件名是config，默认配置文件路劲为当前目录和当前路径下的config目录
func NewDefaultConfig() *Config {
	config := &Config{
		Viper:       viper.New(),
		Name:        configName,
		Paths:       []string{"./", configPath},
		Environment: environment,
	}

	// 获取环境变量
	if os.Getenv("Env") != "" {
		config.Environment = os.Getenv("Env")
	}

	// 配置文件路径
	for _, path := range config.Paths {
		config.AddConfigPath(path)
	}

	return config
}

// ReadConfig 读取配置文件
// needEnv 是否需要合并配置文件，如果true就根据环境变量合并name_env.yaml文件
func (c *Config) ReadConfig(needEnv bool) *Config {
	// 初始化默认配置
	c.SetConfigName(c.Name)
	if err := c.ReadInConfig(); err != nil {
		logger.Logger.Errorf("unable to decode into struct: %s", err)
	}

	// 合并环境变量下的配置文件
	if c.Environment != "" && needEnv {
		c.SetConfigName(c.Name + "." + c.Environment)
		if err := c.MergeInConfig(); err != nil {
			logger.Logger.Errorf("unable to decode into struct: %s", err)
		}
	}

	return c
}

// Bind 初始化配置到cfg对象里面
func (c *Config) Bind(cfg any) error {
	if reflect.TypeOf(cfg).Kind() != reflect.Ptr {
		return errors.New("绑定的参数需要是地址类型")
	}

	if err := c.Unmarshal(cfg); err != nil {
		logger.Logger.Errorf("unable to decode into struct: %s", err)
		return err
	}

	// 监控配置并获取最新配置
	c.WatchConfig()
	// 配置文件修改时触发并解析
	c.OnConfigChange(func(e fsnotify.Event) {
		logger.Logger.Infof("config file changed: %s", e.Name)
		if err := c.Unmarshal(cfg); err != nil {
			logger.Logger.Errorf("%s", err)
		}
	})

	return nil
}

// BindByString 根据configStr字符串读取配置赋值给cfg
func (c *Config) BindByString(configStr string, cfg any) error {
	if reflect.TypeOf(cfg).Kind() != reflect.Ptr {
		return errors.New("绑定的参数需要是地址类型")
	}

	if err := c.MergeConfig(strings.NewReader(configStr)); err != nil {
		return err
	}

	if err := c.Unmarshal(cfg); err != nil {
		logger.Logger.Errorf("unable to decode into struct: %s", err)
		return err
	}
	return nil
}

// Callback 根据configStr字符串读取配置赋值给cfgData
func (c *Config) Callback(configStr string) error {
	if err := c.MergeConfig(strings.NewReader(configStr)); err != nil {
		return err
	}

	return nil
}

// Save 保存配置到文件
func (c *Config) Save() {
	c.WriteConfigAs(c.Name + "." + c.Environment + ".yaml")
}
