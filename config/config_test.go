package config

import (
	"gitee.com/linewise/go-frame/micro/sd/nacos"
	"testing"
)

type testCfg struct {
	Name    string `mapstructure:"name"`
	Version string `mapstructure:"version"`
	Mode    string `mapstructure:"mode"`
}

func TestNewDefaultConfig(t *testing.T) {
	var cfg testCfg
	conf := NewDefaultConfig().ReadConfig(true)
	t.Log(conf.Bind(&cfg))
	t.Log(cfg)
}

func TestNewDefaultConfig1(t *testing.T) {
	var cfg testCfg
	conf := NewDefaultConfig().ReadConfig(false)
	t.Log(conf.Bind(&cfg))
	t.Log(cfg)
}

func TestBindConf(t *testing.T) {
	var cfg testCfg
	conf := NewConfig("", "./examples").ReadConfig(true)
	t.Log(conf.Bind(&cfg))
	t.Log(cfg)
}

func TestConfig_Save(t *testing.T) {
	var cfg testCfg
	conf := NewConfig("", "./examples")
	t.Log(conf.Bind(&cfg))
	t.Log(cfg)

	// 修改配置
	conf.Set("name", "kkk")
	conf.Save()
	t.Log(cfg)
}

func TestConfig_BindByString(t *testing.T) {
	var cfg testCfg
	conf := NewDefaultConfig().ReadConfig(true)
	t.Log(conf.Bind(&cfg))
	t.Log(cfg)

	t.Log(conf.BindByString("version: v1.0.1", &cfg))
	t.Log(cfg)
}

func TestConfig_Callback(t *testing.T) {
	nacosServerConfig := []nacos.ServerConfig{{
		IpAddr:      "127.0.0.1",
		ContextPath: "/nacos",
		Port:        8848,
		Scheme:      "http",
	}}
	nacosClientConfig := nacos.ClientConfig{
		NamespaceId:         "6d3cd430-d4be-4cb2-be27-183ba56746e7",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "./logs",
		CacheDir:            "./caches",
		LogLevel:            "debug",
	}
	nacos, _ := nacos.NewNacos(nacosServerConfig, nacosClientConfig)

	var cfg testCfg
	conf := NewDefaultConfig().ReadConfig(true)
	t.Log(conf.Bind(&cfg))
	t.Log(cfg)

	t.Log(nacos.Listen("db", "", conf.Callback))

	// 程序等待
	var await chan bool = make(chan bool)
	<-await
}
