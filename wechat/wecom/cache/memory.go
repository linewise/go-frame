package cache

import (
	"errors"
	"time"
)

type Memory map[string]struct {
	value  string
	expire time.Time
}

func NewMemory() Memory {
	return make(Memory, 1)
}

func (m Memory) Get(corpId string) (string, error) {
	if v, ok := m[corpId]; ok && time.Now().Before(v.expire) {
		return v.value, nil
	}

	return "", errors.New("时间过期了")
}

func (m Memory) Set(corpId string, accessToken string) error {
	m[corpId] = struct {
		value  string
		expire time.Time
	}{value: accessToken, expire: time.Now().Add(time.Hour * 2)}

	return nil
}
