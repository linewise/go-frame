package cache

import (
	"errors"
	"io"
	"os"
	"path"
	"time"
)

type AccessTokenFile struct {
	filePath string
}

func NewAccessTokenFile() AccessTokenFile {
	return AccessTokenFile{
		filePath: "./tmp/",
	}
}

func (r AccessTokenFile) Get(corpId string) (string, error) {
	if corpId == "" {
		return "", errors.New("corpId值为空")
	}

	tmpName := r.filePath + corpId + ".tmp"
	if _, err := os.Stat(path.Dir(tmpName)); os.IsNotExist(err) {
		return "", errors.New("accessToken值为空")
	}

	fileInfo, err := os.Stat(tmpName)
	if !os.IsNotExist(err) && fileInfo.ModTime().Add(2*time.Hour).Unix() > time.Now().Unix() {
		tmp, err := os.OpenFile(tmpName, os.O_RDWR, os.ModePerm)
		defer tmp.Close()
		if err == nil {
			data, err := io.ReadAll(tmp)
			if err == nil {
				return string(data), nil
			}
		}
	}

	return "", errors.New("accessToken值为空")
}

func (r AccessTokenFile) Set(corpId string, accessToken string) error {
	if corpId == "" || accessToken == "" {
		return errors.New("corpId或accessToken值为空")
	}

	tmpName := r.filePath + corpId + ".tmp"
	if _, err := os.Stat(path.Dir(tmpName)); os.IsNotExist(err) {
		os.MkdirAll(path.Dir(tmpName), os.ModePerm)
	}

	file, err := os.OpenFile(tmpName, os.O_RDWR, os.ModePerm)
	if os.IsNotExist(err) {
		file, err = os.Create(tmpName)
		if err != nil {
			return err
		}
	}

	_, err = file.WriteString(accessToken)
	return err
}
