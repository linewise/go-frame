package cache

import (
	"context"
	"errors"
	"github.com/go-redis/redis/v8"
	"time"
)

type AccessTokenRedis struct {
	client *redis.Client
	Key    string
}

func NewAccessTokenRedis(c *redis.Client) AccessTokenRedis {
	return AccessTokenRedis{
		client: c,
		Key:    "wechat:wecom:access:token:",
	}
}

func (r AccessTokenRedis) Get(corpId string) (string, error) {
	if corpId == "" {
		return "", errors.New("corpId值为空")
	}

	if isExist, err := r.client.Exists(context.Background(), r.Key+corpId).Result(); err != nil || isExist <= 0 {
		return "", errors.New("accessToken不存在")
	}

	accessToken, err := r.client.Get(context.Background(), r.Key+corpId).Result()
	if err != nil && accessToken == "" {
		return "", errors.New("accessToken值为空")
	}

	return accessToken, err
}

func (r AccessTokenRedis) Set(corpId string, accessToken string) error {
	if corpId == "" || accessToken == "" {
		return errors.New("corpId或accessToken值为空")
	}

	expiration := time.Duration(2) * time.Hour
	_, err := r.client.Set(context.Background(), r.Key+corpId, accessToken, expiration).Result()
	return err
}
