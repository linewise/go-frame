package wecom

import (
	"encoding/xml"
	"errors"
	"gitee.com/linewise/go-frame/wechat/wecom/util"
	"io"
)

type (
	// CallbackXml 消息回传业务数据
	CallbackXml struct {
		MsgSignature string `json:"msg_signature"` // 企业微信加密签名
		Timestamp    string `json:"timestamp"`     // 时间戳
		Nonce        string `json:"nonce"`         // 随机数
		ToUserName   string `xml:"ToUserName"`     // 企业微信的CorpId，当为第三方应用回调事件时，CorpId的内容为suiteid
		AgentId      string `xml:"AgentID"`        // 接收的应用id，可在应用的设置页面获取。仅应用相关的回调会带该字段。
		Encrypt      string `xml:"Encrypt"`        // 消息结构体加密后的字符串
	}

	// CallbackMsg 消息事件传入参数
	CallbackMsg struct {
		MsgSignature string    `json:"msg_signature"` // 企业微信加密签名
		Timestamp    string    `json:"timestamp"`     // 时间戳
		Nonce        string    `json:"nonce"`         // 随机数
		Echostr      string    `json:"echostr"`       // 加密的字符串
		XmlReader    io.Reader `json:"xml_reader"`    // XML数据
	}

	Callback struct {
		Token          string `json:"token"`
		EncodingAESKey string `json:"encoding_aes_key"`
	}
)

func (c *CallbackXml) Decode(xmlReader io.Reader) error {
	xmlDecoder := xml.NewDecoder(xmlReader)
	return xmlDecoder.Decode(c)
}

func NewCallback(token, encodingAESKey string) *Callback {
	return &Callback{Token: token, EncodingAESKey: encodingAESKey}
}

// DecryptGet get方式请求数据解析
func (c Callback) DecryptGet(req CallbackMsg) (string, error) {
	wecomCrypto := util.Crypto{Token: c.Token, EncodingAESKey: c.EncodingAESKey}

	// 签名校验
	if req.MsgSignature != wecomCrypto.GetSignature(req.Timestamp, req.Nonce, req.Echostr) {
		return "", errors.New("签名错误")
	}

	// 解密数据
	message, _, err := wecomCrypto.Decrypt(req.Echostr)
	if err != nil {
		return "", err
	}

	// 返回message
	return message, nil
}

// DecryptPost post方式请求数据解析
func (c Callback) DecryptPost(req CallbackMsg) (string, error) {
	wecomCrypto := util.Crypto{Token: c.Token, EncodingAESKey: c.EncodingAESKey}

	// 解析body的数据
	callbackXml := CallbackXml{}
	if err := callbackXml.Decode(req.XmlReader); err != nil {
		return "", err
	}

	// 判断签名
	if req.MsgSignature != wecomCrypto.GetSignature(req.Timestamp, req.Nonce, callbackXml.Encrypt) {
		return "", errors.New("签名错误")
	}

	// 获取消息数据
	message, _, err := wecomCrypto.Decrypt(callbackXml.Encrypt)
	if err != nil {
		return "", errors.New("数据异常")
	}

	// 返回message
	return message, nil
}
