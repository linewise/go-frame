package wecom

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"
)

type (
	SendRobotReturn struct {
		ErrCode int32  `json:"errcode"`
		ErrMsg  string `json:"errmsg"`
	}

	SendRobot struct {
		WebHook string `json:"web_hook"`
	}
)

func NewSendRobot(webHook string) *SendRobot {
	return &SendRobot{
		WebHook: webHook,
	}
}

func (s SendRobot) Send(content map[string]any) error {
	body, err := json.Marshal(content)
	if err != nil {
		return err
	}

	client := &http.Client{}
	req, err := http.NewRequest("POST", s.WebHook, strings.NewReader(string(body)))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err = io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	var r SendRobotReturn
	err = json.Unmarshal(body, &r)
	if err != nil {
		return err
	}
	if r.ErrMsg != "ok" {
		return errors.New(r.ErrMsg)
	}

	return nil
}

func (s SendRobot) SendText(content string) error {
	body, err := json.Marshal(map[string]any{
		"msgtype": "text",
		"text": map[string]string{
			"content": content,
		},
	})
	if err != nil {
		return err
	}

	client := &http.Client{}
	req, err := http.NewRequest("POST", s.WebHook, strings.NewReader(string(body)))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err = io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	var r SendRobotReturn
	err = json.Unmarshal(body, &r)
	if err != nil {
		return err
	}
	if r.ErrMsg != "ok" {
		return errors.New(r.ErrMsg)
	}

	return nil
}
