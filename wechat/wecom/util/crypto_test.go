package util

import "testing"

func TestCrypto_Decrypt(t *testing.T) {
	c := Crypto{
		Token:          "ZfaMbycGq1RKtGSxlnRyNG58E4izCi",
		EncodingAESKey: "oxfg2ONaf7rW4C58RqmuEC31tZptL6b6Skb0Up6HXb4",
	}

	message, id, err := c.Decrypt("/bNrUZ2LSX15ceKp28dqDA42NDsfc0GObRjt6HS7+fSbrnQxbUEOb2vFzfUF0vixdv1HLbZXr15+Jq2wuZTpk/pnIq06vTAlY5c9lZ+MY5nPHlhtGj0jO2PcHgUsZPt+goiuImoopqJQoTLvXqAT3s2H6ZIctUSghRE5I0wknhJIyZwHaJBqfIG7Tq9VY7gEvGxH0zbNXPL2kEMyq6cI1xYvOi7eNNTc9kQs45YcgK2m4wEHxQp6ewePA2Jr1uNC1I/Fb0fGW+1rP8r2KxR10U5uKMzm7wVmrsVO+G+V9n/IBq3hxspydBxYIlXSgLO1uARu/j/idKquTeeqejQ9bBrMWTd6SQBd7QeeM9ccp+8aLVmr34n/Lbd54NXgVs1rRhJWE1ABS2aYgSlfGS8LIvO6kwXqcztO6T7LTlDaW8kggVztUiQ0ldTYqm8I2rnA6mHuPiwq3v49DZze8BmNq3d/FoAfiAXQow7PlxTHwm5Y50gqtaIm1ali/a4Q925ypFHq9jzv/EdJmi6WAL7UojycpOqtqdF7mD8dfEbguOw=")
	t.Log(message)
	t.Log(id)
	t.Log(err)
}
