package util

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha1"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"sort"
	"strings"
)

type Crypto struct {
	Token          string `json:"token"`
	EncodingAESKey string `json:"encoding_aes_key"`
}

// GetSignature 获取签名
// @param {String} timestamp    时间戳
// @param {String} nonce        随机数
// @param {String} encrypt      加密后的文本
func (c Crypto) GetSignature(timeStamp, nonce, encrypt string) string {
	// 将参数值按照字母字典排序，然后从小到大拼接成一个字符串
	signArray := []string{timeStamp, nonce, encrypt, c.Token}
	sort.Strings(signArray)
	sortStr := strings.Join(signArray, "")

	// 返回sha1加密
	return fmt.Sprintf("%x", sha1.Sum([]byte(sortStr)))
}

// Decrypt 对密文进行解密
// 使用AESKey做AES-256-CBC解密
// @param {String} text 待解密的密文
func (c Crypto) Decrypt(text string) (string, string, error) {
	// 解密数据base64解码
	byteText, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", "", err
	}

	// EncodingAESKey使用base64解码
	aesKey, err := base64.StdEncoding.DecodeString(c.EncodingAESKey + "=")
	if err != nil {
		return "", "", err
	}
	if len(aesKey) != 32 {
		return "", "", errors.New("AESKey invalid.")
	}

	// AES采用CBC模式
	deciphered := make([]byte, len(byteText))
	block, err := aes.NewCipher(aesKey)
	if err != nil {
		return "", "", err
	}
	blockMode := cipher.NewCBCDecrypter(block, aesKey[:block.BlockSize()])
	blockMode.CryptBlocks(deciphered, byteText) //将密文byteText解密到deciphered

	// 获取数据
	padding := binary.BigEndian.Uint64(deciphered[16:20])
	message := deciphered[20 : 20+padding]
	id := deciphered[20+padding:]
	return string(message), string(id), nil
}
