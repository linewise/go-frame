package util

import (
	"bytes"
	"io"
	"net/http"
)

type (
	HttpResponse struct {
		ErrCode int64  `json:"errcode"` // 错误信息
		ErrMsg  string `json:"errmsg"`

		AccessToken string `json:"access_token"` // accessToken
		ExpiresIn   int64  `json:"expires_in"`

		Type      string `json:"type"` // media fields
		MediaId   string `json:"media_id"`
		CreatedAt int64  `json:"created_at"`

		Ticket        string `json:"ticket"` // ticket fields
		ExpireSeconds int64  `json:"expire_seconds"`
	}

	HttpClient struct{}
)

func (c HttpClient) Get(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	return io.ReadAll(resp.Body)
}

func (c HttpClient) Post(url string, bodyType string, body *bytes.Buffer) ([]byte, error) {
	resp, err := http.Post(url, bodyType, body)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	return io.ReadAll(resp.Body)
}
