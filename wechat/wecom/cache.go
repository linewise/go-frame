package wecom

type AccessTokenCache interface {
	Get(corpId string) (accessToken string, err error)
	Set(corpId string, accessToken string) error
}
