package wecom

import "testing"

func TestSendRobotText_Send(t *testing.T) {
	err := NewSendRobot("https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=348ac2b9-570a-4532-9964-a75376e87ef7").Send(
		map[string]any{
			"msgtype": "text",
			"text": map[string]string{
				"content": "test",
			},
		},
	)
	t.Log(err)
}
