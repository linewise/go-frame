package wecom

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/linewise/go-frame/wechat/wecom/cache"
	"gitee.com/linewise/go-frame/wechat/wecom/util"
)

type (
	AccessTokenResponse struct {
		ErrCode     int64  `json:"errcode"` // 错误信息
		ErrMsg      string `json:"errmsg"`
		AccessToken string `json:"access_token"`
		ExpiresIn   int64  `json:"expires_in"`
	}

	AccessToken struct {
		AccessTokenCache
	}
)

func NewAccessToken() AccessToken {
	return AccessToken{
		AccessTokenCache: cache.NewMemory(),
	}
}

func (a AccessToken) Get(corpId, secret string) (string, error) {
	// 缓存中查询数据
	accessToken, err := a.AccessTokenCache.Get(corpId)
	if err == nil && accessToken != "" {
		return accessToken, nil
	}

	// 请求获取accessToken
	url := "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + corpId + "&corpsecret=" + secret
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return "", err
	}

	// 解析数据
	response := &AccessTokenResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return "", err
	}
	if response.ErrCode != 0 {
		return "", errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	// 保存到缓存
	return response.AccessToken, a.AccessTokenCache.Set(corpId, response.AccessToken)
}
