package contact

import (
	"encoding/xml"
	"io"
)

type Callback struct {
	ToUserName   string `xml:"ToUserName" json:"-"`   // 企业微信CorpId
	FromUserName string `xml:"FromUserName" json:"-"` // 此事件该值固定为sys，表示该消息由系统生成
	CreateTime   string `xml:"CreateTime" json:"-"`   // 消息创建时间 （整型）
	MsgType      string `xml:"MsgType" json:"-"`      // 消息的类型，此时固定为event
	Event        string `xml:"Event" json:"-"`        // 事件的类型，此时固定为change_contact
	ChangeType   string `xml:"ChangeType" json:"-"`   // 此时固定（添加：create_user，更新：update_user，删除：delete_user）
}

func NewCallback(xmlReader io.Reader) (*Callback, error) {
	c := &Callback{}
	xmlDecoder := xml.NewDecoder(xmlReader)
	return c, xmlDecoder.Decode(c)
}

func (c *Callback) Decode(xmlReader io.Reader) error {
	xmlDecoder := xml.NewDecoder(xmlReader)
	return xmlDecoder.Decode(c)
}
