package contact

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"gitee.com/linewise/go-frame/wechat/wecom/util"
	"io"
	"strconv"
)

type (
	UserExtAttrText struct {
		Value string `json:"value"`
	}

	UserExtAttrWeb struct {
		Url   string `json:"url"`
		Title string `json:"title"`
	}

	UserExtAttr struct {
		Type string          `json:"type"`
		Name string          `json:"name"`
		Text UserExtAttrText `json:"text"`
		Web  UserExtAttrWeb  `json:"web"`
	}

	ExtAttr struct {
		Attrs []UserExtAttr `xml:"Attrs" json:"attrs,omitempty"`
	}

	// User 人员基本信息
	User struct {
		UserId         string   `xml:"UserID" json:"userid,omitempty" validate:"required"` // 成员UserId
		OpenUserId     string   `xml:"OpenUserId" json:"open_userid,omitempty"`            // 成员OpenUserId
		Name           string   `xml:"Name" json:"name,omitempty" validate:"required"`     // 成员名称;代开发自建应用需要管理员授权才返回
		Alias          string   `xml:"Alias" json:"alias,omitempty"`                       // 别名
		Mobile         string   `xml:"Mobile" json:"mobile,omitempty"`                     // 手机号码;代开发自建应用需要管理员授权才返回。上游共享的应用不返回该字段
		Avatar         string   `xml:"Avatar" json:"avatar,omitempty"`                     // 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。上游共享的应用不返回该字段
		Department     []int64  `xml:"Department" json:"department,omitempty"`             // 成员部门列表，仅返回该应用有查看权限的部门id
		Order          []int64  `xml:"Order" json:"order,omitempty"`                       // 部门内的排序值，默认为0，成员次序以创建时间从小到大排列。个数必须和参数department的个数一致，数值越大排序越前面。有效的值范围是[0, 2^32)
		Position       string   `xml:"Position" json:"position,omitempty"`                 // 职位信息。长度为0~64个字节;代开发自建应用需要管理员授权才返回。上游共享的应用不返回该字段
		Gender         string   `xml:"Gender" json:"gender,omitempty"`                     // 性别，1表示男性，2表示女性。上游共享的应用不返回该字段
		Email          string   `xml:"Email" json:"email,omitempty"`                       // 邮箱;代开发自建应用需要管理员授权才返回。上游共享的应用不返回该字段
		BizMail        string   `xml:"BizMail" json:"biz_mail,omitempty"`                  // 企业邮箱;代开发自建应用不返回该字段。上游共享的应用不返回该字段
		Telephone      string   `xml:"Telephone" json:"telephone,omitempty"`               // 座机。32字节以内，由纯数字、“-”、“+”或“,”组成。
		IsLeaderInDept []int64  `xml:"IsLeaderInDept" json:"is_leader_in_dept,omitempty"`  // 表示所在部门是否为部门负责人，0-否，1-是，顺序与Department字段的部门逐一对应。上游共享的应用不返回该字段
		DirectLeader   []string `xml:"DirectLeader" json:"direct_leader,omitempty"`        // 直属上级UserID，最多5个。代开发的自建应用和上游共享的应用不返回该字段
		AvatarMediaId  string   `xml:"AvatarMediaId" json:"avatar_mediaid,omitempty"`      // 成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
		Enable         int64    `xml:"Enable" json:"enable,omitempty"`                     // 启用/禁用成员。1表示启用成员，0表示禁用成员
		ExtAttr        ExtAttr  `xml:"ExtAttr" json:"extattr,omitempty"`                   // 自定义字段
		ToInvite       string   `xml:"ToInvite" json:"to_invite,omitempty"`                // 是否邀请该成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
		//ExternalProfile  string `xml:"ExternalProfile" json:"external_profile,omitempty"`   // 成员对外属性，字段详情见对外属性
		//ExternalPosition string `xml:"ExternalPosition" json:"external_position,omitempty"` // 对外职务，如果设置了该值，则以此作为对外展示的职务，否则以position来展示。长度12个汉字内
		Address        string `xml:"Address" json:"address,omitempty"`      // 地址。长度最大128个字符
		MainDepartment int64  `xml:"MainDepartment" json:"main_department"` // 主部门

		Status int64 `xml:"Status" json:"status,omitempty"` // 激活状态：1=已激活 2=已禁用 4=未激活 已激活代表已激活企业微信或已关注微信插件（原企业号）5=成员退出
	}

	// UserCallback 人员变更回调信息
	UserCallback struct {
		ToUserName   string `xml:"ToUserName" json:"-"`   // 企业微信CorpId
		FromUserName string `xml:"FromUserName" json:"-"` // 此事件该值固定为sys，表示该消息由系统生成
		CreateTime   string `xml:"CreateTime" json:"-"`   // 消息创建时间 （整型）
		MsgType      string `xml:"MsgType" json:"-"`      // 消息的类型，此时固定为event
		Event        string `xml:"Event" json:"-"`        // 事件的类型，此时固定为change_contact
		ChangeType   string `xml:"ChangeType" json:"-"`   // 此时固定（添加：create_user，更新：update_user，删除：delete_user）
		NewUserId    string `xml:"NewUserID" json:"-"`    // 新的UserId，变更时推送（userid由系统生成时可更改一次）
		User
	}

	// UserResponse 返回信息
	UserResponse struct {
		ErrCode int64  `json:"errcode"`
		ErrMsg  string `json:"errmsg"`
	}

	UserModel struct{}
)

func NewUserCallback(xmlReader io.Reader) (*UserCallback, error) {
	u := &UserCallback{}
	xmlDecoder := xml.NewDecoder(xmlReader)
	return u, xmlDecoder.Decode(u)
}

func (u *UserCallback) Decode(xmlReader io.Reader) error {
	xmlDecoder := xml.NewDecoder(xmlReader)
	return xmlDecoder.Decode(u)
}

func NewUser() *UserModel {
	return &UserModel{}
}

// Create 添加人员
func (u UserModel) Create(accessToken string, user User) (errCode int64, err error) {
	if accessToken == "" || user.UserId == "" || user.Name == "" {
		return 0, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=" + accessToken
	userJson, err := json.Marshal(user)
	if err != nil {
		return 0, err
	}

	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(userJson))
	if err != nil {
		return 0, err
	}

	// 解析数据
	response := &UserResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return 0, err
	}
	if response.ErrCode != 0 {
		return response.ErrCode, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return 0, nil
}

// Get 获取人员信息
func (u UserModel) Get(accessToken string, userId string) (user User, errCode int64, err error) {
	if accessToken == "" || userId == "" {
		return User{}, 0, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=" + accessToken + "&userid=" + userId
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return User{}, 0, err
	}

	// 解析数据
	response := &UserResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return User{}, 0, err
	}
	if response.ErrCode != 0 {
		return User{}, response.ErrCode, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return user, 0, json.Unmarshal(msg, &user)
}

// Update 更新人员
func (u UserModel) Update(accessToken string, user User) (errCode int64, err error) {
	if accessToken == "" {
		return 0, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=" + accessToken
	userJson, err := json.Marshal(user)
	if err != nil {
		return 0, err
	}

	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(userJson))
	if err != nil {
		return 0, err
	}

	// 解析数据
	response := &UserResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return 0, err
	}
	if response.ErrCode != 0 {
		return response.ErrCode, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return 0, nil
}

// Del 删除人员
func (u UserModel) Del(accessToken string, userId string) error {
	if accessToken == "" {
		return errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=" + accessToken + "&userid=" + userId
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return err
	}

	// 解析数据
	response := &UserResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return err
	}
	if response.ErrCode != 0 {
		return errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return nil
}

// BatchDel 批量删除人员
func (u UserModel) BatchDel(accessToken string, userIds []string) error {
	if accessToken == "" || len(userIds) <= 0 {
		return errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=" + accessToken
	userJson, err := json.Marshal(
		struct {
			useridlist []string `json:"useridlist"`
		}{
			useridlist: userIds,
		},
	)
	if err != nil {
		return err
	}

	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(userJson))
	if err != nil {
		return err
	}

	// 解析数据
	response := &UserResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return err
	}
	if response.ErrCode != 0 {
		return errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return nil
}

// GroupUserList 获取部门下面的人员
// fetchChild表示是否递归获取子部门下面的成员：1-递归获取，0-只获取本部门
func (u UserModel) GroupUserList(accessToken string, departmentId int64, fetchChild int64) ([]User, error) {
	if accessToken == "" || departmentId <= 0 {
		return nil, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=" + accessToken + "&department_id=" + strconv.Itoa(int(departmentId)) + "&fetch_child=" + strconv.Itoa(int(fetchChild))
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return nil, err
	}

	// 解析数据
	response := &struct {
		UserResponse
		UserList []User `json:"userlist"`
	}{}
	if err := json.Unmarshal(msg, response); err != nil {
		return nil, err
	}
	if response.ErrCode != 0 {
		return nil, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return response.UserList, nil
}

// GroupUserListDetail 获取部门下面的人员详情
// fetchChild表示是否递归获取子部门下面的成员：1-递归获取，0-只获取本部门
func (u UserModel) GroupUserListDetail(accessToken string, departmentId int64, fetchChild int64) ([]User, error) {
	if accessToken == "" || departmentId <= 0 {
		return nil, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=" + accessToken + "&department_id=" + strconv.Itoa(int(departmentId)) + "&fetch_child=" + strconv.Itoa(int(fetchChild))
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return nil, err
	}

	// 解析数据
	response := &struct {
		UserResponse
		UserList []User `json:"userlist"`
	}{}
	if err := json.Unmarshal(msg, response); err != nil {
		return nil, err
	}
	if response.ErrCode != 0 {
		return nil, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return response.UserList, nil
}
