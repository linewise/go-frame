package contact

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/linewise/go-frame/wechat/wecom/util"
	"strconv"
)

type (
	Tag struct {
		TagId   int64  `xml:"TagID" json:"tagid,omitempty"`
		TagName string `xml:"TagName" json:"tagname,omitempty" validate:"required"`
	}

	TagResponse struct {
		ErrCode int64  `json:"errcode"`
		ErrMsg  string `json:"errmsg"`
	}

	TagModel struct{}
)

// Create 创建标签
func (t TagModel) Create(accessToken string, tag Tag) (int64, error) {
	if accessToken == "" {
		return 0, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token=" + accessToken
	tagJson, err := json.Marshal(tag)
	if err != nil {
		return 0, err
	}

	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(tagJson))
	if err != nil {
		return 0, err
	}

	// 解析数据
	response := struct {
		TagResponse
		TagId int64 `json:"tagid,omitempty"`
	}{}
	if err := json.Unmarshal(msg, response); err != nil {
		return 0, err
	}
	if response.ErrCode != 0 {
		return 0, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return response.TagId, nil
}

// Update 更新标签信息
func (t TagModel) Update(accessToken string, tag Tag) error {
	if accessToken == "" {
		return errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token=" + accessToken
	tagJson, err := json.Marshal(tag)
	if err != nil {
		return err
	}

	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(tagJson))
	if err != nil {
		return err
	}

	// 解析数据
	response := &TagResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return err
	}
	if response.ErrCode != 0 {
		return errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return nil
}

// Delete 删除标签信息
func (t TagModel) Delete(accessToken string, tagid int64) error {
	if accessToken == "" || tagid == 0 {
		return errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token=" + accessToken + "&tagid=" + strconv.Itoa(int(tagid))
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return err
	}

	// 解析数据
	response := &TagResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return err
	}
	if response.ErrCode != 0 {
		return errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return nil
}
