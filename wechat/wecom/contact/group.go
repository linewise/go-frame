package contact

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"gitee.com/linewise/go-frame/wechat/wecom/util"
	"io"
)

type (
	Group struct {
		Id               int64    `xml:"Id" json:"id,omitempty" validate:"required"`
		Name             string   `xml:"Name" json:"name,omitempty" validate:"required"`
		NameEn           string   `xml:"NameEn" json:"name_en,omitempty"`                     // 创建部门需要的字段
		DepartmentLeader []string `xml:"DepartmentLeader" json:"department_leader,omitempty"` // 获取部门信息需要的字段
		ParentId         int64    `xml:"ParentId" json:"parentid,omitempty"`
		Order            int64    `xml:"Order" json:"order,omitempty"`
	}

	// GroupCallback 部门变更回传信息
	GroupCallback struct {
		ToUserName   string `xml:"ToUserName"`   // 企业微信CorpId
		FromUserName string `xml:"FromUserName"` // 此事件该值固定为sys，表示该消息由系统生成
		CreateTime   string `xml:"CreateTime"`   // 消息创建时间 （整型）
		MsgType      string `xml:"MsgType"`      // 消息的类型，此时固定为event
		Event        string `xml:"Event"`        // 事件的类型，此时固定为change_contact
		ChangeType   string `xml:"ChangeType"`   // 此时固定（新增：create_party，更新：update_party，删除：delete_party）
		Group
	}

	GroupResponse struct {
		ErrCode int64  `json:"errcode"`
		ErrMsg  string `json:"errmsg"`
	}

	GroupModel struct{}
)

func NewGroupCallback(xmlReader io.Reader) (*GroupCallback, error) {
	g := &GroupCallback{}
	xmlDecoder := xml.NewDecoder(xmlReader)
	return g, xmlDecoder.Decode(g)
}

func (g *GroupCallback) Decode(xmlReader io.Reader) error {
	xmlDecoder := xml.NewDecoder(xmlReader)
	return xmlDecoder.Decode(g)
}

func NewGroup() *GroupModel {
	return &GroupModel{}
}

// Create 创建部门
func (g GroupModel) Create(accessToken string, group Group) (int64, int64, error) {
	var (
		url            = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=" + accessToken
		groupJson, err = json.Marshal(group)
		response       = struct {
			Id int64 `json:"id,omitempty"`
			GroupResponse
		}{}
	)

	if accessToken == "" {
		return 0, 0, errors.New("缺少参数")
	}
	if err != nil {
		return 0, 0, err
	}
	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(groupJson))
	if err != nil {
		return 0, 0, err
	}

	// 解析数据
	if err := json.Unmarshal(msg, &response); err != nil {
		return 0, 0, err
	}
	if response.ErrCode != 0 {
		return 0, response.ErrCode, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return response.Id, 0, nil
}

// Update 更新部门信息
func (g GroupModel) Update(accessToken string, group Group) (errCode int64, err error) {
	if accessToken == "" {
		return 0, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=" + accessToken
	groupJson, err := json.Marshal(group)
	if err != nil {
		return 0, err
	}

	msg, err := util.HttpClient{}.Post(url, "text/plain", bytes.NewBuffer(groupJson))
	if err != nil {
		return 0, err
	}

	// 解析数据
	response := &GroupResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return 0, err
	}
	if response.ErrCode != 0 {
		return response.ErrCode, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return 0, nil
}

// Delete 删除部门信息
func (g GroupModel) Delete(accessToken string, id string) error {
	if accessToken == "" || id == "" {
		return errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=" + accessToken + "&id=" + id
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return err
	}

	// 解析数据
	response := &GroupResponse{}
	if err := json.Unmarshal(msg, response); err != nil {
		return err
	}
	if response.ErrCode != 0 {
		return errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return nil
}

// Get 获取部门
// 获取指定部门及其下的子部门（以及子部门的子部门等等，递归）。如果不填，默认获取全量组织架构
func (g GroupModel) Get(accessToken string, id string) (Group, error) {
	if accessToken == "" || id == "" {
		return Group{}, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/department/get?access_token=" + accessToken + "&id=" + id
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return Group{}, err
	}

	// 解析数据
	response := struct {
		GroupResponse
		Department Group `json:"department,omitempty"`
	}{}
	if err := json.Unmarshal(msg, response); err != nil {
		return Group{}, err
	}
	if response.ErrCode != 0 {
		return Group{}, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return response.Department, nil
}

// GetList 获取部门列表
// 获取指定部门及其下的子部门（以及子部门的子部门等等，递归）。如果不填，默认获取全量组织架构
func (g GroupModel) GetList(accessToken string, id string) ([]Group, error) {
	if accessToken == "" {
		return nil, errors.New("缺少参数")
	}

	url := "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=" + accessToken
	if id != "" {
		url = url + "&id=" + id
	}
	msg, err := util.HttpClient{}.Get(url)
	if err != nil {
		return nil, err
	}

	// 解析数据
	response := struct {
		GroupResponse
		Department []Group `json:"department,omitempty"`
	}{}
	if err := json.Unmarshal(msg, &response); err != nil {
		return nil, err
	}
	if response.ErrCode != 0 {
		return nil, errors.New(fmt.Sprintf("企业微信返回错误：%d %s", response.ErrCode, response.ErrMsg))
	}

	return response.Department, nil
}
