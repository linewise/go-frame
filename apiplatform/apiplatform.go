package apiplatform

import (
	"encoding/json"
	"errors"
	"gitee.com/linewise/go-frame/apiplatform/cache"
	"io"
	"net/http"
	"strings"
)

type (
	Config struct {
		Url      string `json:"url" mapstructure:"url"`
		Account  string `json:"account" mapstructure:"account"`
		Password string `json:"password" mapstructure:"password"`
	}

	ApiPlatform struct {
		Config
		TokenCache
	}
)

// NewApiPlatform 接口平台调用
func NewApiPlatform(config Config) *ApiPlatform {
	return &ApiPlatform{
		Config:     config,
		TokenCache: cache.NewMemory(),
	}
}

func (a ApiPlatform) GetToken() (string, error) {
	token, err := a.TokenCache.Get(a.Account)
	if err == nil && token != "" {
		return token, err
	}

	urlStr := a.Config.Url + "/v1/public/user/getToken"
	payload := strings.NewReader(`{"login": "` + a.Config.Account + `", "password": "` + a.Config.Password + `"}`)
	resp, err := http.Post(urlStr, "application/json", payload)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	var dataMap map[string]any
	if err := json.NewDecoder(resp.Body).Decode(&dataMap); err != nil {
		return "", err
	}

	return dataMap["token"].(string), a.TokenCache.Set(a.Account, dataMap["token"].(string))
}

func (a ApiPlatform) CallApi(companyId, tagName, paramsJson string) ([]byte, error) {
	if !json.Valid([]byte(paramsJson)) {
		return nil, errors.New("传入参数不是json")
	}

	// 获取token
	token, err := a.GetToken()
	if err != nil {
		return nil, err
	}

	// 请求数据
	client := &http.Client{}
	urlStr := a.Config.Url + "/v1/public/api/call"
	payload := strings.NewReader(`{"company_id": "` + companyId + `", "tagName": "` + tagName + `", "params": ` + paramsJson + `}`)
	req, err := http.NewRequest("POST", urlStr, payload)
	req.Header.Set("b-json-web-token", token)
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	return io.ReadAll(resp.Body)
}
