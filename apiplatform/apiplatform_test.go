package apiplatform

import (
	"gitee.com/linewise/go-frame/apiplatform/cache"
	"testing"
)

func TestApiPlatform_GetToken(t *testing.T) {
	apiPlatform := ApiPlatform{
		Config: Config{
			Url:      "https://api-platform-rd.mobimedical.cn/api",
			Account:  "admin",
			Password: "098ikl;po",
		},
		TokenCache: cache.NewMemory(),
	}

	t.Log(apiPlatform.GetToken())
}

func TestApiPlatform_CallApi(t *testing.T) {
	apiPlatform := ApiPlatform{
		Config: Config{
			Url:      "https://api-platform-rd.mobimedical.cn/api",
			Account:  "admin",
			Password: "098ikl;po",
		},
		TokenCache: cache.NewMemory(),
	}

	t.Log(apiPlatform.CallApi("6037074e5573ca396105f269", "test", `{"content": "wanglongjun"}`))
}
