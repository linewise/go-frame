package cache

import (
	"errors"
	"time"
)

type Memory map[string]struct {
	value  string
	expire time.Time
}

func NewMemory() Memory {
	return make(Memory, 1)
}

func (m Memory) Get(account string) (string, error) {
	if v, ok := m[account]; ok && time.Now().Before(v.expire) {
		return v.value, nil
	}

	return "", errors.New("时间过期了")
}

func (m Memory) Set(account string, token string) error {
	m[account] = struct {
		value  string
		expire time.Time
	}{value: token, expire: time.Now().Add(time.Hour * 2)}

	return nil
}
