package cache

import (
	"errors"
	"io"
	"os"
	"path"
	"time"
)

type Fs struct {
	filePath string
}

func NewFs() *Fs {
	return &Fs{
		filePath: "./tmp/",
	}
}

func (f Fs) Get(account string) (string, error) {
	if account == "" {
		return "", errors.New("账号值为空")
	}

	tmpName := f.filePath + account + ".tmp"
	if _, err := os.Stat(path.Dir(tmpName)); os.IsNotExist(err) {
		return "", errors.New("accessToken值为空")
	}

	fileInfo, err := os.Stat(tmpName)
	if !os.IsNotExist(err) && fileInfo.ModTime().Add(2*time.Hour).Unix() > time.Now().Unix() {
		tmp, err := os.OpenFile(tmpName, os.O_RDWR, os.ModePerm)
		defer tmp.Close()
		if err == nil {
			data, err := io.ReadAll(tmp)
			if err == nil {
				return string(data), nil
			}
		}
	}

	return "", errors.New("token值为空")
}

func (f Fs) Set(account string, token string) error {
	if account == "" || token == "" {
		return errors.New("账号或token值为空")
	}

	tmpName := f.filePath + account + ".tmp"
	if _, err := os.Stat(path.Dir(tmpName)); os.IsNotExist(err) {
		os.MkdirAll(path.Dir(tmpName), os.ModePerm)
	}

	file, err := os.OpenFile(tmpName, os.O_RDWR, os.ModePerm)
	if os.IsNotExist(err) {
		file, err = os.Create(tmpName)
		if err != nil {
			return err
		}
	}

	_, err = file.WriteString(token)
	return err
}
